from ibastro import fitslibs, coordtrans
import sys

with open('/Users/ibojicic/data/PROJECTS/astro/LMC_Beta/base_catalogue_positions.csv', 'r') as base_cat:
    base_cat_lines = base_cat.readlines()

inimage = '/Users/ibojicic/data/PROJECTS/astro/LMC_Beta/MWA_for_rms/Week2_white_lownoise_ddmod_rms_rescaled.fits'

with open('/Users/ibojicic/data/PROJECTS/astro/LMC_Beta/gleam_rmss.csv', 'w') as gleam_res:
    for line in base_cat_lines:
        id, ra, dec = line.rstrip().split(',')
        print(id)

        id = int(id)
        ra = float(ra)
        dec = float(dec)
        pixX, pixY = coordtrans.coord2pix(inimage, ra, dec)
        rms = fitslibs.valueAtCoord(inimage, pixX - 1, pixY - 1)
        gleam_res.write('{},{}\n'.format(id,rms))

        print('{}: {} {} {} {} {}'.format(id, ra, dec, rms, pixX - 1, pixY - 1))

