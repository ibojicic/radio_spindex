import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import pandas as pd
import numpy as np
from importlib import reload
import make_diam_image
from ibplotting import plotseds as ibpltsed
from ibcommon import dicts as ibdict
from ibfitting import ThermalSed, PowerLaw
from ibplotting import PlotSed

# from scipy.stats import multivariate_normal
# from mpl_toolkits import mplot3d
# import math
# import matplotlib.pyplot as plt
# import numpy as np

ibplt = reload(ibpltsed)
model = reload(model)
ThermalSed = reload(ThermalSed)
PowerLaw = reload(PowerLaw)
PlotSed = reload(PlotSed)
make_diam_image = reload(make_diam_image)

tbl_radio_data = model.RadioData
tbl_results = model.PneFitResults

query_radio = tbl_radio_data.select()
df_radio = pd.DataFrame(list(query_radio.dicts()))

all_ids = df_radio.idPNMain.unique()

all_ids.sort()

query_meta = Gpnfullview.select(
    Gpnfullview.png,
    Gpnfullview.idPNMain,
    Gpnfullview.draj2000,
    Gpnfullview.ddecj2000,
    Gpnfullview.p_nstat,
    Gpnfullview.name,
    Gpnfullview.maj_diam,
    Gpnfullview.min_diam,
    Gpnfullview.reftb_ang_diam
)

df_meta = pd.DataFrame(list(query_meta.dicts()))


# all_ids = [94, 202, 325, 388, 406, 501, 639, 730, 741, 752, 775, 780, 824, 1097, 1144]
all_ids = [730]
models = ['sphshell', 'trpowerlawmod', 'cylindrical']
# models = ['sphshell']

id_lim = 1

models_config = {
    'sphshell'     : {
        'lntp'    : '-',
        'clr'     : '#990000',
        'plt_conf': True},
    'trpowerlawmod': {
        'lntp'    : 'dashdot',
        'clr'     : '#006600',
        'plt_conf': False},
    'cylindrical'  : {
        'lntp'    : '--',
        'clr'     : '#000066',
        'plt_conf': False}
}

for curr_id in all_ids:

    if curr_id < id_lim:
        continue
    print("Working on {}".format(curr_id))
    # select specific object
    # for fitting use only InUse = 'y'
    data_radio = df_radio.loc[(df_radio['idPNMain'] == curr_id) & (df_radio['InUse'] == 'y'),]
    data_meta = df_meta.loc[df_meta['idPNMain'] == curr_id]

    # for unavailable uncertanties use 10%
    data_radio.flux_err.fillna(data_radio.flux / 10., inplace=True)

    unq_bands = data_radio.band.unique()

    if len(unq_bands) < 3:
        continue

    newnewplot = PlotSed.PlotSed()
    newnewplot.set_config_pars(xlabel='Freq (GHz)', ylabel='Flux (mJy)')
    newnewplot.set_limits_on_data(data_radio.freq, data_radio.flux)
    newnewplot.plt_data_points(data_radio, True)
    all_res = []

    # check if at least one fit converged
    fit_flag = False



    for mdl in models:
        fit_params = {
            "model": mdl,
            "Te"   : 1.E4,
            "mu"   : 0.4
        }
        # fitter = ThermalSed.ThermalSed(fit_params)
        # fitter.set_data_pandas(data_radio, xcol='freq', ycol='flux', ycol_err='flux_err', beams='bmaj')
        # fitter.run_fit()

        # tbl_results.create(idPNMain=curr_id, **fitter.full_results)

        # if fitter.fit_results is None:
        #     continue
        # fit_flag = True

        # newnewplot.plt_thermal_sed(data_radio.freq,
        #                            fitter.fit_results,
        #                            fitted_err=fitter.fit_errors,
        #                            **models_config[mdl])
        # all_res.append(fitter.full_results)

    # linear_fitter = PowerLaw.PowerLaw(data_radio, 'freq', 'flux', yobs_err='flux_err', fittype='curvefit')
    # results_linear_fitter = linear_fitter.run_fit()
    # results_linear_fitter = ibdict.merge_dicts([results_linear_fitter.full_results,
    #                                             {
    #                                                 'model': 'powerlaw'
    #                                                 }
    #                                             ])
    # tbl_results.create(idPNMain=curr_id, **results_linear_fitter)

    # if fit_flag:
    #     newplt = newnewplot.save_plot(newnewplot.plot_out(), "/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/sedplots/{}_sed.png".format(curr_id))
    #     make_diam_image.make_diam_image(curr_id, data_meta, all_res)
