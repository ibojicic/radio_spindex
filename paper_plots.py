import sys
import numpy as np
import pandas as pd
from ibplotting import ggPlots, PlotBasics, PlotStats, PlotCombine
import plotnine as gg
from importlib import reload
import load_data as ld
import paper_fitting as pf
import ibplotting.markers as mrkr

reload(ld)
reload(pf)
reload(ggPlots)
reload(PlotStats)
reload(PlotBasics)

crcl = mrkr.markCircle()
navy_blue = '#000080'


def pwrlaw(x, intercept, slope):
    return (10. ** intercept) * (x ** slope)


pathtopaper = "/Users/ibojicic/data/papers/SEDPNe_new/"

plot_test = "{}plots/test.png".format(pathtopaper)
plot_test1 = "{}plots/test1.eps".format(pathtopaper)

plot_thetatheta_spshell = "{}plots/thetatheta_spshell.eps".format(pathtopaper)
plot_thetatheta_plshell = "{}plots/thetatheta_plshell.eps".format(pathtopaper)
plot_thetatheta_spshell_opt = "{}plots/thetatheta_spshell_opt.eps".format(pathtopaper)
plot_thetatheta_plshell_opt = "{}plots/thetatheta_plshell_opt.eps".format(pathtopaper)

plot_theta6cm = "{}plots/theta6cm.eps".format(pathtopaper)
plot_blandaltman = "{}plots/blandaltman.eps".format(pathtopaper)
plot_blandaltman_olrl = "{}plots/blandaltman_olrl.eps".format(pathtopaper)
plot_blandaltman_olrl_10perc = "{}plots/blandaltman_olrl_10perc.eps".format(pathtopaper)
plot_blandaltman_sep = "{}plots/blandaltman_sep.eps".format(pathtopaper)

plot_blandaltman_opt = "{}plots/blandaltman_opt.eps".format(pathtopaper)
plot_blandaltman_opt_sep = "{}plots/blandaltman_opt_sep.eps".format(pathtopaper)

plot_blandaltman_10perc = "{}plots/blandaltman_10perc.eps".format(pathtopaper)
plot_blandaltman_avgvis = "{}plots/blandaltman_avgvis.eps".format(pathtopaper)
plot_blandaltman_deconv = "{}plots/blandaltman_deconv.eps".format(pathtopaper)

plot_thetatheta_rl_hash = "{}plots/thetatheta_rl_hash.eps".format(pathtopaper)
plot_blandaltman_rl_hash = "{}plots/blandaltman_rl_hash.eps".format(pathtopaper)

plot_thetatheta_rl_opt = "{}plots/thetatheta_rl_opt.eps".format(pathtopaper)
plot_blandaltman_rl_opt = "{}plots/blandaltman_rl_opt.eps".format(pathtopaper)

plot_thetatheta_lit = "{}plots/thetatheta_lit.eps".format(pathtopaper)
plot_blandaltman_lit = "{}plots/blandaltman_lit.eps".format(pathtopaper)

plot_thetatheta_frewrad = "{}plots/thetatheta_frewrad.eps".format(pathtopaper)
plot_blandaltman_frewrad = "{}plots/blandaltman_frewrad.eps".format(pathtopaper)

plot_thetatheta_ruff = "{}plots/thetatheta_ruff.eps".format(pathtopaper)
plot_blandaltman_ruff = "{}plots/blandaltman_ruff.eps".format(pathtopaper)

plot_em_rad = "{}plots/em_rad.eps".format(pathtopaper)
plot_em_rad_combined = "{}plots/em_rad_comb.eps".format(pathtopaper)
plot_em_theta = "{}plots/em_theta.eps".format(pathtopaper)
plot_dist_histo = "{}plots/dist_histo.eps".format(pathtopaper)

histo_rnd_em_r_rsquared = "{}plots/rnd_EM_R_rsquared.eps".format(pathtopaper)
histo_rnd_em_r_pearson = "{}plots/rnd_EM_R_pearson.eps".format(pathtopaper)

histo_radio_fluxes = "{}plots/histo_radio_fluxes.eps".format(pathtopaper)
histo_pwrlaw = "{}plots/histo_power_law.eps".format(pathtopaper)

################################################################
# copy dfs that you need
################################################################
df_all_diams = ld.df_all_diams.copy()
df_full_data_qlimited = ld.df_full_data_qlimited.copy()
df_full_data = ld.df_full_data.copy()
df_thetatheta_emr_use = ld.df_thetatheta_emr_use.copy()
df_thetatheta_emr_nouse = ld.df_thetatheta_emr_nouse.copy()
df_thetatheta_emr_new = ld.df_thetatheta_emr_new.copy()
df_sphshell_flt = ld.df_sphshell_flt.copy()
df_radio_all_merged = ld.df_results_merged.copy()
df_hst_vs_tylenda = ld.df_hst_vs_tylenda.copy()

###### FROM HERE ######


# ################################################################
# # theta theta RL vs OL & medians combined
# ################################################################
#
# all_data_limits = (0.3, 500)
#
# plot_top = PlotBasics.PlotBasics('loglog')
# plot_top.data = df_all_diams[~df_all_diams.method.isna()].copy()
# plot_top.set_config_pars(xlabel='OL Diameter (arcsec)',
#                          ylabel='RL Diameter (arcsec)',
#                          legend_pos=(0.23, 0.86))
# plot_top.plot_xy('DiamOpt', 'DiamRad', color='black', aes_shape='method', size=2)
# plot_top.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                           drop=True, )
# plot_top.plot_oneone(linetype='dotted')
# plot_top.xlimits = all_data_limits
# plot_top.ylimits = all_data_limits
#
# plot_bottom = PlotStats.PlotStats('loglin')
# plot_bottom.data = df_all_diams.copy()
# plot_bottom.set_config_pars(xlabel='OL Diameter (arcsec)',
#                             ylabel='Mean\nDiff(%)',
#                             logthick_length=0.03)
#
# plot_bottom.bland_altman_medians('DiamOpt', 'DiamRad', diffplot='percovermean', no_bins=6, colour='black')
# plot_bottom.plot_hline(0, linetype='dotted')
# plot_bottom.plot_hline(np.nanmedian(plot_bottom.data['ba_diff']), linetype='dashed')
# plot_bottom.xlimits = all_data_limits
# plot_bottom.ylimits = (-30, 10)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_two(plot_top, plot_bottom)
# combined_plot.save_plot(combined, plot_thetatheta_rl_opt)
#
# ################################################################
# # Bland Altman RL vs OL
# ################################################################
#
#
# ba_plot = PlotStats.PlotStats('loglin')
# ba_plot.data = df_all_diams[~df_all_diams.method.isna()].copy()
# ba_plot.set_config_pars(xlabel='Mean (arcsec)',
#                         ylabel='Difference (%)',
#                         legend_pos='none')
# # ba_plot.bland_altman_plot('DiamRad', 'theta_x', diffplot='perc', extend_x=1.3, aes_fill='factor(quality)')
# ba_plot.bland_altman_plot('DiamOpt', 'DiamRad', diffplot='percovermean', extend_x=1.3,
#                           aes_shape='method', size=3)  # , plot_ids='idPNMain')
#
# ba_plot.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                          drop=True, )
#
# ba_plot.save_plot(ba_plot.plot_out(), plot_blandaltman_olrl)
#
# ################################################################
# # Bland Altman RL vs OL for 10% radio
# ################################################################
#
#
# ba_plot = PlotStats.PlotStats('loglin')
# ba_plot.data = df_all_diams[df_all_diams.method == '10perc'].copy()
# ba_plot.set_config_pars(xlabel='Mean (arcsec)',
#                         ylabel='Difference (%)',
#                         legend_pos='none')
# # ba_plot.bland_altman_plot('DiamRad', 'theta_x', diffplot='perc', extend_x=1.3, aes_fill='factor(quality)')
# ba_plot.bland_altman_plot('DiamOpt', 'DiamRad', diffplot='percovermean', extend_x=1.3,
#                           aes_shape='method', size=3)  # , plot_ids='idPNMain')
#
# ba_plot.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                          drop=True, )
#
# ba_plot.save_plot(ba_plot.plot_out(), plot_blandaltman_olrl_10perc)
#
# ################################################################
# # END theta theta RL vs OL & medians combined
# ################################################################
#
# ################################################################
# # theta theta SED ~ RL plot
# ################################################################
#
# df_full_data_plot = df_full_data.copy()
#
# df_full_data_plot['qlt_x'] = np.array(['<4' if a < 4 else '>3' for a in df_full_data_plot['quality_x']]).astype('str')
# df_full_data_plot['qlt_y'] = np.array(['<4' if a < 4 else '>3' for a in df_full_data_plot['quality_y']]).astype('str')
#
# ################################################################
# # sp shell
# ################################################################
#
# plot = PlotBasics.PlotBasics('loglog')
# plot.data = df_full_data_plot[~df_full_data_plot.method.isna()]
#
# plot.set_config_pars(xlabel='Lit Radio Diameter (arcsec)',
#                      ylabel='SED Radio Diameter (arcsec)',
#                      legend_pos=(0.23, 0.76))
#
# plot.plot_xy('DiamRad', 'theta_x', aes_shape='method', aes_color='factor(qlt_x)', size=2). \
#     plot_errorbars(xcol='DiamRad', ycol='theta_x', yerr='theta_err_x', width=0.01)
#
# plot.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                       drop=True, )
#
# plot.elements = gg.scale_color_manual(name='quality', values={'<4': 'red',
#                                                               '>3': 'blue'
#                                                               },
#                                       drop=True)
#
# plot.plot_oneone(linetype='dotted')
# plot.xlimits = (0.3, 100)
# plot.ylimits = (0.3, 100)
#
# # plot.plot_ids('idPNMain','DiamRad','theta_x')
#
# plot_bottom = PlotStats.PlotStats('loglin')
# plot_bottom.set_config_pars(xlabel='RL Diameter (arcsec)',
#                             ylabel='Mean\nDiff(%)',
#                             logthick_length=0.03)
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_x == '<4'].copy()
# plot_bottom.bland_altman_medians('DiamRad', 'theta_x', diffplot='percovermean', no_bins=6, colour='red')
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_x == '>3'].copy()
# plot_bottom.bland_altman_medians('DiamRad', 'theta_x', diffplot='percovermean', no_bins=6, colour='blue',
#                                  linetype='dashed')
#
# plot_bottom.plot_hline(0, linetype='dotted')
# # plot_bottom.plot_hline(np.nanmedian(plot_bottom.data['ba_diff'] ), linetype='dashed')
# plot_bottom.xlimits = all_data_limits
# plot_bottom.xlimits = (0.3, 100)
# plot_bottom.ylimits = (-60, 20)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_two(plot, plot_bottom)
# combined_plot.save_plot(combined, plot_thetatheta_spshell)
#
# ################################################################
# # pl shell
# ################################################################
#
# plot = PlotBasics.PlotBasics('loglog')
# plot.data = df_full_data_plot[~df_full_data_plot.method.isna()]
#
# plot.set_config_pars(xlabel='Lit Radio Diameter (arcsec)',
#                      ylabel='SED Radio Diameter (arcsec)',
#                      legend_pos=(0.25, 0.76))
#
# plot.plot_xy('DiamRad', 'theta_y', aes_shape='method', aes_color='factor(qlt_y)', size=2). \
#     plot_errorbars(xcol='DiamRad', ycol='theta_y', yerr='theta_err_y', width=0.01)
#
# plot.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                       drop=True, )
#
# plot.elements = gg.scale_color_manual(name='quality', values={'<4': 'red',
#                                                               '>3': 'blue'
#                                                               },
#                                       drop=True)
# # plot ids for check
# # plot.data = df_full_data_plot[df_full_data_plot.idPNMain.isin([74,483, 965, 1144, 1215, 1278, 4103])]
# # plot.plot_ids('idPNMain', 'DiamRad', 'theta_y')
#
# plot.plot_oneone(linetype='dotted')
# plot.xlimits = (0.1, 100)
# plot.ylimits = (0.1, 100)
#
# plot_bottom = PlotStats.PlotStats('loglin')
# plot_bottom.set_config_pars(xlabel='RL Diameter (arcsec)',
#                             ylabel='Mean\nDiff(%)',
#                             logthick_length=0.03)
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_y == '<4'].copy()
# plot_bottom.bland_altman_medians('DiamRad', 'theta_y', diffplot='percovermean', no_bins=6, colour='red')
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_y == '>3'].copy()
# plot_bottom.bland_altman_medians('DiamRad', 'theta_y', diffplot='percovermean', no_bins=6, colour='blue',
#                                  linetype='dashed')
#
# plot_bottom.plot_hline(0, linetype='dotted')
# # plot_bottom.plot_hline(np.nanmedian(plot_bottom.data['ba_diff'] ), linetype='dashed')
# plot_bottom.xlimits = all_data_limits
# plot_bottom.xlimits = (0.1, 100)
# plot_bottom.ylimits = (-125, 5)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined_plot.sub_left = 0.14
# combined = combined_plot.vertical_plot_combine_two(plot, plot_bottom)
# combined_plot.save_plot(combined, plot_thetatheta_plshell)
#
# ################################################################
# # END theta theta SED ~ RL plot
# ################################################################
#
#
# ################################################################
# # theta theta SED ~ OL plot
# ################################################################
#
# # add extra column for qualities
#
# df_full_data_plot = df_full_data.copy()
#
# df_full_data_plot['qlt_x'] = np.array(['<4' if a < 4 else '>3' for a in df_full_data_plot['quality_x']]).astype('str')
# df_full_data_plot['qlt_y'] = np.array(['<4' if a < 4 else '>3' for a in df_full_data_plot['quality_y']]).astype('str')
#
# ################################################################
# # sp shell
# ################################################################
#
# plot = PlotBasics.PlotBasics('loglog')
# plot.data = df_full_data_plot[~df_full_data_plot.ref_optdiam.isna()]
#
# plot.set_config_pars(xlabel='OL Diameter (arcsec)',
#                      ylabel='SED Radio Diameter (arcsec)',
#                      legend_pos=(0.23, 0.76))
#
# plot.plot_xy('DiamOpt', 'theta_x', aes_shape='ref_optdiam', size=2, aes_color='factor(qlt_x)'). \
#     plot_errorbars(xcol='DiamOpt', ycol='theta_x', yerr='theta_err_x', color=navy_blue, width=0.01)
#
# plot.elements = gg.scale_shape_manual(name='OL ref.', values={'Tylenda2003': crcl,
#                                                               'Ruffle2004' : 'x',
#                                                               'this paper' : '.'},
#                                       drop=True)
#
# plot.elements = gg.scale_color_manual(name='quality', values={'<4': 'red',
#                                                               '>3': 'blue'
#                                                               },
#                                       drop=True)
#
# # plot.plot_ids('idPNMain','DiamOpt','theta_x')
#
# plot.plot_oneone(linetype='dotted')
# plot.xlimits = (0.3, 100)
# plot.ylimits = (0.3, 100)
#
# plot_bottom = PlotStats.PlotStats('loglin')
# plot_bottom.set_config_pars(xlabel='OL Diameter (arcsec)',
#                             ylabel='Mean\nDiff(%)',
#                             logthick_length=0.03)
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_x == '<4'].copy()
# plot_bottom.bland_altman_medians('DiamOpt', 'theta_x', diffplot='percovermean', no_bins=6, colour='red')
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_x == '>3'].copy()
# plot_bottom.bland_altman_medians('DiamOpt', 'theta_x', diffplot='percovermean', no_bins=6, colour='blue',
#                                  linetype='dashed')
#
# plot_bottom.plot_hline(0, linetype='dotted')
# # plot_bottom.plot_hline(np.nanmedian(plot_bottom.data['ba_diff'] ), linetype='dashed')
# plot_bottom.xlimits = all_data_limits
# plot_bottom.xlimits = (0.3, 100)
# plot_bottom.ylimits = (-100, 20)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_two(plot, plot_bottom)
# combined_plot.save_plot(combined, plot_thetatheta_spshell_opt)
#
# ################################################################
# # pl shell
# ################################################################
#
# plot = PlotBasics.PlotBasics('loglog')
# plot.data = df_full_data_plot[~df_full_data_plot.ref_optdiam.isna()]
#
# plot.set_config_pars(xlabel='OL Diameter (arcsec)',
#                      ylabel='SED Radio Diameter (arcsec)',
#                      legend_pos=(0.23, 0.76))
#
# plot.plot_xy('DiamOpt', 'theta_y', aes_shape='ref_optdiam', size=2, aes_color='factor(qlt_y)'). \
#     plot_errorbars(xcol='DiamOpt', ycol='theta_y', yerr='theta_err_y', color=navy_blue, width=0.01)
#
# plot.elements = gg.scale_shape_manual(name='OL ref.', values={'Tylenda2003': crcl,
#                                                               'Ruffle2004' : 'x',
#                                                               'this paper' : '.'},
#                                       drop=True)
#
# plot.elements = gg.scale_color_manual(name='quality', values={'<4': 'red',
#                                                               '>3': 'blue'},
#                                       drop=True)
# # plot ids for check
# # plot.data = df_full_data_plot[df_full_data_plot.idPNMain.isin([74,483, 965, 1144, 1215, 1278, 4103])]
# # plot.plot_ids('idPNMain', 'DiamOpt', 'theta_y')
#
# plot.plot_oneone(linetype='dotted')
# plot.xlimits = (0.1, 100)
# plot.ylimits = (0.1, 100)
#
# plot_bottom = PlotStats.PlotStats('loglin')
# plot_bottom.set_config_pars(xlabel='OL Diameter (arcsec)',
#                             ylabel='Mean\nDiff(%)',
#                             logthick_length=0.03)
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_y == '<4'].copy()
# plot_bottom.bland_altman_medians('DiamOpt', 'theta_y', diffplot='percovermean', no_bins=6, colour='red')
#
# plot_bottom.data = df_full_data_plot[df_full_data_plot.qlt_y == '>3'].copy()
# plot_bottom.bland_altman_medians('DiamOpt', 'theta_y', diffplot='percovermean', no_bins=6, colour='blue',
#                                  linetype='dashed')
#
# plot_bottom.plot_hline(0, linetype='dotted')
# # plot_bottom.plot_hline(np.nanmedian(plot_bottom.data['ba_diff'] ), linetype='dashed')
# plot_bottom.xlimits = all_data_limits
# plot_bottom.xlimits = (0.3, 100)
# plot_bottom.ylimits = (-125, 5)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_two(plot, plot_bottom)
# combined_plot.save_plot(combined, plot_thetatheta_plshell_opt)
#
# ################################################################
# # bland altman radio plot
# ################################################################
#
# badata = df_full_data_qlimited[~df_full_data_qlimited.method.isna()]
#
# ba_plot = PlotStats.PlotStats('loglin')
# ba_plot.data = badata
# ba_plot.set_config_pars(xlabel='Mean (arcsec)',
#                         ylabel='Difference (%)',
#                         legend_pos='none')
# # ba_plot.bland_altman_plot('DiamRad', 'theta_x', diffplot='perc', extend_x=1.3, aes_fill='factor(quality)')
# ba_plot.bland_altman_plot('DiamRad', 'theta_x', diffplot='percovermean', extend_x=1.3,
#                           aes_shape='method', size=3)  # , plot_ids='idPNMain')
#
# ba_plot.elements = gg.scale_shape_manual(name='RL method', values={'10perc': crcl, 'avgvis': 'x', 'deconv': '.'},
#                                          drop=True, )
#
# ba_plot.save_plot(ba_plot.plot_out(), plot_blandaltman)
#
# ################################################################
# # bland altman radio plots by method
# ################################################################
#
# # 10perc
# ba_plot_10perc = PlotStats.PlotStats('loglin')
# ba_plot_10perc.data = df_full_data_qlimited[df_full_data_qlimited.method == '10perc']
# ba_plot_10perc.set_config_pars(xlabel='Mean (arcsec)',
#                                ylabel='',
#                                legend_pos='none')
# ba_plot_10perc.bland_altman_plot('DiamRad', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape=crcl,
#                                  show_sdmarker=False)
# ba_plot_10perc.xlimits = (0.5, 50)
# ba_plot_10perc.ylimits = (-60, 60)
#
# # ba_plot_10perc.save_plot(ba_plot_10perc.plot_out(), plot_blandaltman_10perc)
#
# # avgvis
# ba_plot_avgvis = PlotStats.PlotStats('loglin')
# ba_plot_avgvis.data = df_full_data_qlimited[df_full_data_qlimited.method == 'avgvis']
# ba_plot_avgvis.set_config_pars(xlabel='Mean (arcsec)',
#                                ylabel='Difference (%)',
#                                legend_pos='none')
# ba_plot_avgvis.bland_altman_plot('DiamRad', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape='x',
#                                  show_sdmarker=False)
# ba_plot_avgvis.xlimits = (0.5, 50)
# ba_plot_avgvis.ylimits = (-50, 50)
#
# # ba_plot_avgvis.save_plot(ba_plot_avgvis.plot_out(), plot_blandaltman_avgvis)
#
# # deconv
# ba_plot_deconv = PlotStats.PlotStats('loglin')
# ba_plot_deconv.data = df_full_data_qlimited[df_full_data_qlimited.method == 'deconv']
# ba_plot_deconv.set_config_pars(xlabel='Mean (arcsec)',
#                                ylabel='',
#                                legend_pos='none')
# ba_plot_deconv.bland_altman_plot('DiamRad', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape='.',
#                                  show_sdmarker=False)
# ba_plot_deconv.xlimits = (0.5, 50)
# ba_plot_deconv.ylimits = (-120, 120)
#
# # ba_plot_deconv.save_plot(ba_plot_deconv.plot_out(), plot_blandaltman_deconv)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_three(ba_plot_10perc, ba_plot_avgvis, ba_plot_deconv)
# combined_plot.save_plot(combined, plot_blandaltman_sep)
#
# ################################################################
# # bland altman optical plot
# ################################################################
#
# badata = df_full_data_qlimited[~df_full_data_qlimited.ref_optdiam.isna()]
#
# ba_opt_plot = PlotStats.PlotStats('loglin')
# ba_opt_plot.data = badata
# ba_opt_plot.set_config_pars(xlabel='Mean (arcsec)',
#                             ylabel='Difference (%)',
#                             legend_pos='none')
# # ba_plot.bland_altman_plot('DiamRad', 'theta_x', diffplot='perc', extend_x=1.3, aes_fill='factor(quality)')
# ba_opt_plot.bland_altman_plot('DiamOpt', 'theta_x', diffplot='percovermean', extend_x=1.3,
#                               aes_shape='ref_optdiam', size=3)  # , plot_ids='idPNMain')
#
# ba_opt_plot.elements = gg.scale_shape_manual(name='OL method',
#                                              values={'Ruffle2004': crcl, 'Tylenda2003': 'x', 'this paper': '.'},
#                                              drop=True, )
#
# ba_opt_plot.save_plot(ba_opt_plot.plot_out(), plot_blandaltman_opt)
#
# ################################################################
# # bland altman optical plots by reference
# ################################################################
#
# # Ruffle 2004
# ba_plot_R04 = PlotStats.PlotStats('loglin')
# ba_plot_R04.data = df_full_data_qlimited[df_full_data_qlimited.ref_optdiam == 'Ruffle2004']
# ba_plot_R04.set_config_pars(xlabel='Mean (arcsec)',
#                             ylabel='',
#                             legend_pos='none')
# ba_plot_R04.bland_altman_plot('DiamOpt', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape=crcl,
#                               show_sdmarker=False)
# ba_plot_R04.xlimits = (0.5, 50)
# ba_plot_R04.ylimits = (-40, 30)
#
# # ba_plot_10perc.save_plot(ba_plot_10perc.plot_out(), plot_blandaltman_10perc)
#
# # Tylenda 2003
# ba_plot_T03 = PlotStats.PlotStats('loglin')
# ba_plot_T03.data = df_full_data_qlimited[df_full_data_qlimited.ref_optdiam == 'Tylenda2003']
# ba_plot_T03.set_config_pars(xlabel='Mean (arcsec)',
#                             ylabel='Difference (%)',
#                             legend_pos='none')
# ba_plot_T03.bland_altman_plot('DiamOpt', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape='x',
#                               show_sdmarker=False)
# ba_plot_T03.xlimits = (0.5, 50)
# ba_plot_T03.ylimits = (-200, 60)
#
# # ba_plot_avgvis.save_plot(ba_plot_avgvis.plot_out(), plot_blandaltman_avgvis)
#
# # HST
# ba_plot_HST = PlotStats.PlotStats('loglin')
# ba_plot_HST.data = df_full_data_qlimited[df_full_data_qlimited.ref_optdiam == 'this paper']
# ba_plot_HST.set_config_pars(xlabel='Mean (arcsec)',
#                             ylabel='',
#                             legend_pos='none')
# ba_plot_HST.bland_altman_plot('DiamOpt', 'theta_x', diffplot='percovermean', extend_x=1.3, size=3, shape='.',
#                               show_sdmarker=False)
# ba_plot_HST.xlimits = (0.5, 50)
# ba_plot_HST.ylimits = (-100, 100)
#
# # ba_plot_deconv.save_plot(ba_plot_deconv.plot_out(), plot_blandaltman_deconv)
#
# ############# combine it ###############
#
# combined_plot = PlotCombine.PlotCombine()
# combined = combined_plot.vertical_plot_combine_three(ba_plot_R04, ba_plot_T03, ba_plot_HST)
# combined_plot.save_plot(combined, plot_blandaltman_opt_sep)
#
# ################################################################
# # EM - R plot
# ################################################################
#
###### TO HERE ######


plot_em_r = PlotStats.PlotStats('loglog')
plot_em_r.set_config_pars(xlabel='Radius (pc)',
                          ylabel='EM ($\mathregular{10^6}$ pc cm$\mathregular{^{-6}}$)',
                          legend_pos='none')

plot_em_r.data = df_full_data_qlimited[df_full_data_qlimited.PhRad_x.notna()]

plot_em_r.plot_xy('PhRad_x', 'EM_x', symmetric=False, shape='o', color='black', aes_fill='factor(distmeth)')

plot_em_r.elements = gg.scale_fill_manual(name='dist. method', values={'S': 'white', 'P': 'black'},
                                          drop=True, )

plot_em_r.plot_pwr_law('PhRad_x', 10 ** pf.odr_fitter_R_EM.full_results['intersect'],
                       pf.odr_fitter_R_EM.full_results['power'],
                       linetype='dashed')
plot_em_r.plot_pwr_law('PhRad_x', 10 ** pf.odr_fitter_R_EM_noerr.full_results['intersect'],
                       pf.odr_fitter_R_EM_noerr.full_results['power'],
                       linetype='dotted')
plot_em_r.plot_errorbars('PhRad_x', 'EM_x', 'PhRad_err_x', 'EM_err_x', height=0.05, width=0.03, size=0.02)

plot_em_r.xlimits = (3e-3, 0.3)
plot_em_r.ylimits = (0.05, 3e3)

plot_em_r.save_plot(plot_em_r.plot_out(), plot_em_rad)

################################################################
# used distances histogram
################################################################

df_full_data_qlimited['dkpc'] = df_full_data_qlimited.d / 1E3

plot_distances_histo = PlotStats.PlotStats('linlin')
plot_distances_histo.set_config_pars(xlabel='D (kpc)',
                                     ylabel='Count',
                                     axis_text_size=12,
                                     axis_title_size=16,
                                     lintick_drct=None,
                                     legend_pos='none')

plot_distances_histo.data = df_full_data_qlimited[
    df_full_data_qlimited.PhRad_x.notna() & df_full_data_qlimited.d.notna()]
plot_distances_histo.plot_histo('dkpc', linetype='solid', bins=20, color='black', fill='white')  # , binwidth=10)
plot_distances_histo.xlimits = (0, 13)
plot_distances_histo.ylimits = (0, 21)

plot_distances_histo.save_plot(plot_distances_histo.plot_out(), plot_dist_histo)

combined_plot = PlotCombine.PlotCombine()
combined = combined_plot.inset_plot_combine_two(plot_em_r, plot_distances_histo,
                                                inset_conf=[0.69, 0.69, 0.25, 0.25])
combined_plot.save_plot(combined, plot_em_rad_combined)

sys.exit()

################################################################
# EM - theta plot
################################################################

plot_em_r = PlotBasics.PlotBasics('loglog')
plot_em_r.set_config_pars(xlabel='Angular Diam (arcsec)',
                          ylabel='EM ($\mathregular{10^6}$ pc cm$\mathregular{^{-6}}$)',
                          legend_pos='none')

plot_em_r.data = df_full_data_qlimited[df_full_data_qlimited.PhRad_x.notna()]
plot_em_r.plot_xy('theta_x', 'EM_x', symmetric=False, shape='o', color='black')

plot_em_r.plot_pwr_law('theta_x', 10 ** pf.odr_fitter_theta_EM.full_results['intersect'],
                       pf.odr_fitter_theta_EM.full_results['power'],
                       linetype='dashed')
# plot_em_r.plot_pwr_law('theta_x', 10 ** pf.odr_fitter_theta_EM_noerr.full_results['intersect'],
#                        pf.odr_fitter_theta_EM_noerr.full_results['power'],
#                        linetype='dotted')
plot_em_r.plot_errorbars('theta_x', 'EM_x', 'theta_err_x', 'EM_err_x', height=0.05, width=0.02, size=0.02)

plot_em_r.data = df_full_data_qlimited[df_full_data_qlimited.PhRad_x.isna()]
plot_em_r.plot_xy('theta_x', 'EM_x', symmetric=False, shape='.', color='red')

plot_em_r.data = df_sphshell_flt
plot_em_r.plot_xy('theta', 'EM', symmetric=False, shape='.', color='red')

# plot_em_r.xlimits = (4e-1, 70)
# plot_em_r.ylimits = (1e4, 1e9)

# plot_em_r.plot_ids('idPNMain','theta_x','EM_x')
plot_em_r.save_plot(plot_em_r.plot_out(), plot_em_theta)

sys.exit()

# # Create linear regression object
# regr_EM_phdiam = linear_model.LinearRegression()
#
# x_data = np.log10(np.array(list(df_thetatheta_emr_use.PhRad_x))).reshape([-1, 1])
# y_data = np.log10(np.array(list(df_thetatheta_emr_use.EM_x)))
# # Train the model using the training sets
# regr_EM_phdiam.fit(x_data, y_data)
#
# # The coefficients
# print('R^2: {}'.format(regr_EM_phdiam.score(x_data, y_data)))
# print('Intercept: {}'.format(regr_EM_phdiam.intercept_))
# print('Slope: {}'.format(regr_EM_phdiam.coef_))
# print(pearsonr(df_thetatheta_emr_use.PhRad_x, df_thetatheta_emr_use.EM_x))
#
# # Create linear regression object
# regr_EM_theta = linear_model.LinearRegression()
# x_data = np.log10(np.array(list(df_thetatheta_emr_use.theta_x))).reshape([-1, 1])
# y_data = np.log10(np.array(list(df_thetatheta_emr_use.EM_x)))
# # Train the model using the training sets
# regr_EM_theta.fit(x_data, y_data)
# # The coefficients
# print('R^2: {}'.format(regr_EM_theta.score(x_data, y_data)))
# print('Intercept: {}'.format(regr_EM_theta.intercept_))
# print('Slope: {}'.format(regr_EM_theta.coef_))
# print(pearsonr(df_thetatheta_emr_use.theta_x, df_thetatheta_emr_use.EM_x))


################################################################
# sed theta 6cm flux plot
################################################################

# # 568
# flux_b = 4909
# theta_b = 10.4
# # 730
# flux_f = 483
# theta_f = 44.4
#
# def flux_theta_scale(init_flux,init_theta,D):
#     return init_flux/D**2, init_theta/D
#
# distances = np.arange(1e-3,100,10)
#
# fluxes_b,thetas_b = flux_theta_scale(flux_b,theta_b,distances)
# fluxes_f,thetas_f = flux_theta_scale(flux_f,theta_f,distances)
#
# df_bright = pd.DataFrame({'flux':fluxes_b,'theta':thetas_b})
# df_faint = pd.DataFrame({'flux':fluxes_f,'theta':thetas_f})

plot_th6cm = PlotBasics.PlotBasics('loglog')
plot_th6cm.set_config_pars(xlabel='SED Radio Diameter (arcsec)',
                           ylabel='Flux 5GHz (mJy)')
plot_th6cm.data = df_full_data_qlimited[df_full_data_qlimited.DiamRad.notna()]
plot_th6cm.plot_xy('theta_x', 'flux', color=navy_blue, shape='o')  # .plot_ids('idPNMain','theta_x','flux')
plot_th6cm.data = df_full_data_qlimited[df_full_data_qlimited.DiamRad.isna()]
plot_th6cm.plot_xy('theta_x', 'flux', color='black', shape='.', size=2)  # .plot_ids('idPNMain','theta_x','flux')

# plot_th6cm.elements = gg.geom_line(data=df_bright, mapping=gg.aes(x='theta',y='flux'))
# plot_th6cm.elements = gg.geom_line(data=df_faint, mapping=gg.aes(x='theta',y='flux'))

plot_th6cm.xlimits = (1e-1, 1e2)
plot_th6cm.ylimits = (1, 1e4)
plot_th6cm.save_plot(plot_th6cm.plot_out(), plot_theta6cm)

################################################################
# Powerlaw spindex distributions
################################################################

radio_fluxes_histo = PlotStats.PlotStats('linlin')
radio_fluxes_histo.data = ld.df_powerlaw
# radio_fluxes_histo.plot_stat_bin('power', linetype='solid', binwidth=0.075)
radio_fluxes_histo.plot_density('power', linetype='solid')
radio_fluxes_histo.elements = gg.geom_vline(xintercept=
                                            np.median(ld.df_powerlaw['power']),
                                            linetype='dotted',
                                            colour='blue')

# print(np.median(ld.df_powerlaw['power']))
radio_fluxes_histo.data = ld.df_powerlaw_cross
# radio_fluxes_histo.plot_stat_bin('power', linetype='dashdot', binwidth=0.075)
radio_fluxes_histo.plot_density('power', linetype='dashdot')
radio_fluxes_histo.elements = gg.geom_vline(xintercept=
                                            np.median(ld.df_powerlaw_cross['power']),
                                            linetype='dotted',
                                            colour='red')
# print(np.median(ld.df_powerlaw_cross['power']))
radio_fluxes_histo.xlimits = (-0.5, 1)
radio_fluxes_histo.ylimits = (0, 1.2)
# rnd_EM_R_histo.elements = gg.geom_vline(xintercept=pf.curvefit_fitter_R_EM.full_results['rsqared'], linetype='dashed')
radio_fluxes_histo.save_plot(radio_fluxes_histo.plot_out(), histo_radio_fluxes)

################################################################
# EM - R histogram with random Diams
################################################################

# rnd_EM_R_histo = PlotStats.PlotStats('linlin')
# rnd_EM_R_histo.data = pf.df_random_EM_R
# rnd_EM_R_histo.plot_histo('rsqared', bins=30, color='black', fill='white')
# rnd_EM_R_histo.ylimits = (0, 20)
# rnd_EM_R_histo.elements = gg.geom_vline(xintercept=pf.curvefit_fitter_R_EM.full_results['rsqared'], linetype='dashed')
# rnd_EM_R_histo.save_plot(rnd_EM_R_histo.plot_out(), histo_rnd_em_r_rsquared)
#
# rnd_EM_R_histo = PlotStats.PlotStats('linlin')
# rnd_EM_R_histo.data = pf.df_random_EM_R
# rnd_EM_R_histo.plot_histo('pearson', bins=30, color='black', fill='white')
# rnd_EM_R_histo.ylimits = (0, 20)
# rnd_EM_R_histo.elements = gg.geom_vline(xintercept=pf.curvefit_fitter_R_EM.full_results['pearson'], linetype='dashed')
# rnd_EM_R_histo.save_plot(rnd_EM_R_histo.plot_out(), histo_rnd_em_r_pearson)
