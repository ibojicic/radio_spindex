import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import pandas as pd
from importlib import reload
pd.options.mode.chained_assignment = None
from ibplotting import PlotSed
import make_diam_image

PlotSed = reload(PlotSed)
make_diam_image = reload(make_diam_image)
model = reload(model)

path_to_fits = "/Users/ibojicic/data/fitsImages/PN_images/"

tbl_radio_data = model.RadioData
tbl_results = model.PneFitResults

query_radio = tbl_radio_data.select()
df_radio = pd.DataFrame(list(query_radio.dicts()))
# df_radio['flux_min'] = df_radio['flux']-df_radio['flux_err']
# df_radio['flux_max'] = df_radio['flux']+df_radio['flux_err']

query_results = tbl_results.select().where(tbl_results.theta.is_null(False))
df_results = pd.DataFrame(list(query_results.dicts()))

all_ids = df_results.idPNMain.unique()

all_ids.sort()

query_meta = Gpnfullview.select(
        Gpnfullview.png,
        Gpnfullview.idPNMain,
        Gpnfullview.draj2000,
        Gpnfullview.ddecj2000,
        Gpnfullview.p_nstat,
        Gpnfullview.name,
        Gpnfullview.maj_diam,
        Gpnfullview.min_diam,
        Gpnfullview.reftb_ang_diam
        )

df_meta = pd.DataFrame(list(query_meta.dicts()))

# test = [19,23,28,37]
#
# test_radio = df_radio.loc[df_radio['idPNMain'].isin(test),]
# test_results = df_results.loc[df_results['idPNMain'].isin(test),]
#
all_ids = [730]
models = ['sphshell', 'trpowerlawmod', 'cylindrical']

id_lim = 1

models_config = {
    'sphshell'     : {
        'lntp'    : '-',
        'clr'     : '#990000',
        'plt_conf': True},
    'trpowerlawmod': {
        'lntp'    : 'dashdot',
        'clr'     : '#006600',
        'plt_conf': False},
    'cylindrical'  : {
        'lntp'    : '--',
        'clr'     : '#000066',
        'plt_conf': False}
    }


for curr_id in all_ids:

    print("Working on {}".format(curr_id))

    data_radio = df_radio.loc[(df_radio['idPNMain'] == curr_id) & (df_radio['InUse'] == 'y'),]
    data_meta = df_meta.loc[df_meta['idPNMain'] == curr_id]
    data_results = df_results.loc[df_results['idPNMain'] == curr_id]

    # for unavailable uncertanties use 10%
    data_radio.flux_err.fillna(data_radio.flux / 10., inplace=True)

    newnewplot = PlotSed.PlotSed()
    newnewplot.set_config_pars(xlabel='Freq (GHz)', ylabel='Flux (mJy)')
    newnewplot.set_limits_on_data(data_radio.freq, data_radio.flux)
    newnewplot.plt_data_points(data_radio, True)
    all_res = []

    for index,row in data_results.iterrows():
        fit_params = row[['model','Te','ni']]
        fit_results = row[['theta','freq_0']]
        fit_errors = row[['theta_err','freq_0_err']]

        newnewplot.plt_thermal_sed(data_radio.freq,
                                   fit_params,
                                   fit_results,
                                   fitted_err=fit_errors,
                                   **models_config[row['model']])
        all_res.append(row)

    newplt = newnewplot.save_plot(newnewplot.plot_out(), "/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/sedplots/{}_sed.png".format(curr_id))

    ## create diam images for each available fits
    make_diam_image.make_diam_image(curr_id, data_meta, all_res)


