import os
import datetime
import math
from importlib import reload
# from load_data import *
import load_data as ld
import sys

reload(ld)

pathtopaper = "/Users/ibojicic/data/papers/SEDPNe_new/"
parsfile = "{}files/parameters.tex".format(pathtopaper)


def f0(x):
    return "{:.0f}".format(x)


def f1(x):
    return "{:.1f}".format(x)


def f2(x):
    return "{:.2f}".format(x)


def pafit(x):
    return "{:.0f}".format(x - 90)


def writepars(var, val, comment, file=parsfile):
    text = "\\newcommand{{\\{0}}}{{{1}}} %{2}\n".format(var, val, comment)
    with open(file, "a") as f:
        f.write(text)


os.remove(parsfile)
with open(parsfile, "w") as f:
    f.write("% parameters file {}\n".format(datetime.datetime.now()))

## copy dfs that you need

df_vla_fitted_diams_avg = ld.df_vla_fitted_diams_avg.copy()
df_vla_fitted_diams = ld.df_vla_fitted_diams.copy()
df_radiodiamdata_avg = ld.df_radiodiamdata_avg.copy()

df_radio_data_used = ld.df_radio_data_used.copy()
df_full_data = ld.df_full_data.copy()
# df_full_data_use = ld.df_full_data_qlimited.copy()

# cross section between new VLA diams and literature radio diams
df_radio_diams_cross = df_radiodiamdata_avg[df_radiodiamdata_avg.idPNMain.isin(list(df_vla_fitted_diams.idPNMain))].shape[0]

# numbers for new VLA diams
writepars("NoFittedVLAImages", df_vla_fitted_diams.shape[0], "total images accessed for diams")
writepars("NoNewVLADiams", df_vla_fitted_diams_avg.idPNMain.nunique(), "number of newly fitted angular diams in VLA")
writepars("NoVLAOverlap", df_radio_diams_cross, "number of fitted angular diams already in the literature")


## radio data with more one or more radio point
writepars("NoWithRadio", ld.total_radio_counts, "number of pne with at least 1 flux density in different bands")

## radio datta with more than 2 data points
writepars("NoFoundObjects", df_radio_data_used.idPNMain.nunique(), "number of pne with at least 3 flux densities in different bands")

## number of SED fitted diams
spshell_notna = df_full_data[df_full_data.theta_x.notna()].copy()
plshell_notna = df_full_data[df_full_data.theta_y.notna()].copy()

no_SED_diams_sphshell = spshell_notna.idPNMain.nunique()
writepars("NoSEDfittedspshell", no_SED_diams_sphshell, "number of pne with succesful SED fit for sp shell")

no_SED_diams_plshell = plshell_notna.idPNMain.nunique()
writepars("NoSEDfittedplshell", no_SED_diams_plshell, "number of pne with succesful SED fit for pl shell")

no_RL_and_spshell_diams = spshell_notna[spshell_notna.DiamRad.notna()].idPNMain.nunique()
writepars("noRLandspshelldiams", no_RL_and_spshell_diams, "number of pne with both spshell and RL diam")

no_RL_and_plshell_diams = plshell_notna[plshell_notna.DiamRad.notna()].idPNMain.nunique()
writepars("noRLandplshelldiams", no_RL_and_plshell_diams, "number of pne with both plshell and RL diam")

no_OL_and_spshell_diams = spshell_notna[spshell_notna.DiamOpt.notna()].idPNMain.nunique()
writepars("noOLandspshelldiams", no_OL_and_spshell_diams, "number of pne with both spshell and OL diam")

no_OL_and_plshell_diams = plshell_notna[plshell_notna.DiamOpt.notna()].idPNMain.nunique()
writepars("noOLandplshelldiams", no_OL_and_plshell_diams, "number of pne with both plshell and OL diam")



## number of MWA detected PN in Gleam 1 and 2
df_mwa_detections = df_radio_data_used[df_radio_data_used.instrument == 'mwa']
no_gleam_det = df_mwa_detections[df_mwa_detections.reference != 'Ivan']['idPNMain'].nunique()
no_ivanmwa_det = df_mwa_detections[df_mwa_detections.reference == 'Ivan']['idPNMain'].nunique()
writepars("NoGleamDet", no_gleam_det, "number of pne detected in Gleam 1&2 catalogued")
writepars("NoIvanGleamDet", no_ivanmwa_det, "number of pne detected in Gleam 1&2 measured")
writepars("NoMWADetected", no_gleam_det + no_ivanmwa_det, "total number of pne detected in Gleam ")
writepars("NoMWAMeasured", no_gleam_det + no_ivanmwa_det - 3, "total number of pne measured in Gleam ")

## list of references for the radio data
radio_references = ",".join(list(df_radio_data_used.bibtex.unique()))
writepars("RadioReferences", radio_references, "radio references")

## number of searched in MWA
writepars("TotalT", 3119, "total number of T in HASH")
writepars("TotalTinMWA", 2857, "total number of T in HASH covered by MWA")
writepars("TotalMWASearched", 407, "total number of T in HASH searched for emission")
writepars("TotalMWARejected", 2857 - 407, "total number of T in HASH rejected")
writepars("TotalMWARejectedPerc", int(round((2857 - 407) / 2857 * 100, 0)),
          "total number of T in HASH rejected in perc")
writepars("MWADetected", no_gleam_det + no_ivanmwa_det, "total number of MWA detected")
writepars("MWADetectedPerc", int(math.ceil((no_gleam_det + no_ivanmwa_det) / 2857 * 100)),
          "total number of MWA detected in perc")
