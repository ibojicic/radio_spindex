from importlib import reload
import load_data as ld
from ibfitting import PowerLaw
from ibastro import physics as ibph
import pandas as pd
import numpy as np
from ibfitting import forecasting_metrics as ibmetr

reload(ld)
reload(PowerLaw)

################################################################
# copy dfs that you need
################################################################

df_full_data_qlimited = ld.df_full_data_qlimited[~(ld.df_full_data_qlimited.PhRad_x.isna())].copy()
df_full_data = ld.df_full_data.copy()

fit_params_crv = {
    "model"  : 'powerlaw',
    "fittype": 'curvefit'
}

fit_params_odr = {
    "model"  : 'powerlaw',
    "fittype": 'odr'
}


################################################################
# fit PhRad_x ~ EM_x with uncertainties
################################################################

# ODR fitter
# https://docs.scipy.org/doc/scipy/reference/odr.html
odr_fitter_R_EM = PowerLaw.PowerLaw(fit_params_odr)
odr_fitter_R_EM.set_data_pandas(df_full_data_qlimited, xcol='PhRad_x', ycol='EM_x',
                                xcol_err='PhRad_err_x', ycol_err='EM_err_x')
odr_fitter_R_EM.run_fit()

# curve_fit
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
curvefit_fitter_R_EM = PowerLaw.PowerLaw(fit_params_crv)
curvefit_fitter_R_EM.set_data_pandas(df_full_data_qlimited, xcol='PhRad_x', ycol='EM_x',
                                     ycol_err='EM_err_x')
curvefit_fitter_R_EM.run_fit()

################################################################
# fit PhRad_x ~ EM_x without uncertainties
################################################################

# ODR fitter
# https://docs.scipy.org/doc/scipy/reference/odr.html
odr_fitter_R_EM_noerr = PowerLaw.PowerLaw(fit_params_odr)
odr_fitter_R_EM_noerr.set_data_pandas(df_full_data_qlimited, xcol='PhRad_x', ycol='EM_x')
odr_fitter_R_EM_noerr.run_fit()

# curve_fit
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
curvefit_fitter_R_EM_noerr = PowerLaw.PowerLaw(fit_params_crv)
curvefit_fitter_R_EM_noerr.set_data_pandas(df_full_data_qlimited, xcol='PhRad_x', ycol='EM_x')
curvefit_fitter_R_EM_noerr.run_fit()

################################################################
# fit theta_x ~ EM_x with uncertainties
################################################################

# ODR fitter
# https://docs.scipy.org/doc/scipy/reference/odr.html
odr_fitter_theta_EM = PowerLaw.PowerLaw(fit_params_odr)
odr_fitter_theta_EM.set_data_pandas(df_full_data_qlimited, xcol='theta_x', ycol='EM_x',
                                    xcol_err='theta_err_x', ycol_err='EM_err_x')
odr_fitter_theta_EM.run_fit()

# curve_fit
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
curvefit_fitter_theta_EM = PowerLaw.PowerLaw(fit_params_crv)
curvefit_fitter_theta_EM.set_data_pandas(df_full_data_qlimited, xcol='theta_x', ycol='EM_x',
                                         ycol_err='EM_err_x')
curvefit_fitter_theta_EM.run_fit()

################################################################
# fit theta_x ~ EM_x without uncertainties
################################################################

# ODR fitter
# https://docs.scipy.org/doc/scipy/reference/odr.html
odr_fitter_theta_EM_noerr = PowerLaw.PowerLaw(fit_params_odr)
odr_fitter_theta_EM_noerr.set_data_pandas(df_full_data_qlimited, xcol='theta_x', ycol='EM_x')
odr_fitter_theta_EM_noerr.run_fit()

# curve_fit
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
curvefit_fitter_theta_EM_noerr = PowerLaw.PowerLaw(fit_params_crv)
curvefit_fitter_theta_EM_noerr.set_data_pandas(df_full_data_qlimited, xcol='theta_x', ycol='EM_x')
curvefit_fitter_theta_EM_noerr.run_fit()
# results_curvefit_theta_EM_noerr = curvefit_fitter_theta_EM_noerr.run_fit()

################################################################
#Pearson correlation
################################################################

pearson_corr = ibmetr.pearson_corr(np.log10(df_full_data_qlimited.PhRad_x), np.log10(df_full_data_qlimited.EM_x))

################################################################
# fit theta_x ~ flux for available lit DiamRad without uncertainties
################################################################

# data_flux_radio = df_thetatheta_emr_use[(df_thetatheta_emr_use.DiamRad.notna()) &
#                                         (df_thetatheta_emr_use.flux.notna())]
#
# # curve_fit
# # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
# curvefit_fitter_theta_flux = PowerLaw.PowerLaw(data_flux_radio,
#                                                'theta_x', 'flux', fittype='curvefit')
# results_curvefit_theta_flux = curvefit_fitter_theta_flux.run_fit()

################################################################
# fit theta_x ~ flux for all available without uncertainties
################################################################

# data_flux_radio_all = df_full_data[(df_full_data.flux.notna()) &
#                                    (df_full_data.theta_x.notna())]
#
# # curve_fit
# # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
# curvefit_fitter_theta_flux_all = PowerLaw.PowerLaw(data_flux_radio_all,
#                                                    'theta_x', 'flux', fittype='curvefit')
# results_curvefit_theta_flux_all = curvefit_fitter_theta_flux_all.run_fit()

################################################################
# check EM-R fit with randomised distances
################################################################

# rnd_rsqared = []
# rnd_pearson = []
#
# for i in range(100):
#     dist = np.random.permutation(df_thetatheta_emr_use.d)
#     theta = df_thetatheta_emr_use.theta_x
#     EM = df_thetatheta_emr_use.EM_x
#
#     PhRad = ibph.radius(dist, theta)[0] / 1000
#
#     df_random = pd.DataFrame({'EM': EM, 'PhRad': PhRad})
#
#     # curve_fit
#     # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
#     random_R_EM_fitter = PowerLaw.PowerLaw(df_random, 'PhRad', 'EM', fittype='curvefit')
#     random_R_EM = random_R_EM_fitter.run_fit()
#     rnd_rsqared.append(random_R_EM.full_results['rsqared'])
#     rnd_pearson.append(random_R_EM.full_results['pearson'])
#
# df_random_EM_R = pd.DataFrame({'rsqared': rnd_rsqared, 'pearson': rnd_pearson})
