import ibastro.fitslibs as ibfits
from peeweedbmodels.dbSED_PNe import FF_DiamImages, FitRadioDiams
from peeweedbmodels.dbMainGPN import Gpnfullview
import ibastro.montages as ibmont
import os
import numpy as np
import pandas as pd
from astropy.io import fits
from photutils.isophote import EllipseGeometry, Ellipse
from ibcommon import parse

os.environ['MPLCONFIGDIR'] = '/tmp'
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.ioff()

import aplpy
from matplotlib.path import Path

def diam_image(inputFile,
               inRA,
               inDEC,
               percmin=1,
               percmax=99.5,
               valmin=None,
               valmax=None,
               imsize=False,
               stretch_scale='linear',
               arcsinh_strech=None,
               add_text=None,
               textsize=28,
               addBeam=False,
               doNorth=False,
               figSize=(8.67, 7.79)
               ):
    fig = plt.figure(figsize=figSize)
    fig.subplots_adjust(bottom=0.1, left=0.12, top=0.95, right=0.92)

    markrs = aplpy.FITSFigure(inputFile, figure=fig, convention='calabretta', north=doNorth)

    markrs.recenter(inRA, inDEC, radius=imsize)

    markrs.show_grayscale(invert='True',
                          pmin=percmin,
                          pmax=percmax,
                          vmin=valmin,
                          vmax=valmax,
                          stretch=stretch_scale,
                          vmid=arcsinh_strech)

    markrs.axis_labels.hide()
    markrs.tick_labels.hide()

    diam_round = parse.round_to_n(imsize * 3600. / 2., 2)
    markrs.add_scalebar(diam_round / 3600.)
    markrs.scalebar.set_corner('top left')
    markrs.scalebar.set_font_size(16)
    markrs.scalebar.set_label("{} arcsec".format(diam_round))

    if add_text is not None:
        markrs.add_label(0.5, 0.08, text=add_text, size=textsize, relative=True, backgroundcolor="white")

    if addBeam:
        markrs.add_beam()
        markrs.beam.set_color('black')
        markrs.beam.set_hatch('+')

    return markrs, fig


def add_contour(marker, cntr_levels, cntr_file, color='white', linestyle='-', linewidth=0.8):
    marker.show_contour(cntr_file, levels=cntr_levels, colors=color, convention='calabretta',
                        linestyles=linestyle, linewidths=linewidth)

def pixlen2coordlen(inimage, length, length_pa):
    delts = ibfits.getHeaderItems(inimage, ['CDELT1', 'CDELT2'])

    length_x = length * np.cos(np.radians(length_pa)) * abs(delts['CDELT1']) * 3600
    length_y = length * np.sin(np.radians(length_pa)) * abs(delts['CDELT2']) * 3600

    res = np.sqrt(length_x ** 2 + length_y ** 2)
    return res

def coordlen2pixlen(inimage, length, length_pa):
    delts = ibfits.getHeaderItems(inimage, ['CDELT1', 'CDELT2'])

    length_x = length * np.cos(np.radians(length_pa)) / (abs(delts['CDELT1']) * 3600)
    length_y = length * np.sin(np.radians(length_pa)) / (abs(delts['CDELT2']) * 3600)

    res = np.sqrt(length_x ** 2 + length_y ** 2)
    return res


query_images = FF_DiamImages.select().where(FF_DiamImages.fit_diam == 'y')
df_images = pd.DataFrame(list(query_images.dicts()))

query_meta = Gpnfullview.select(
        Gpnfullview.png,
        Gpnfullview.idPNMain,
        Gpnfullview.draj2000,
        Gpnfullview.ddecj2000,
        Gpnfullview.p_nstatus,
        Gpnfullview.name,
        Gpnfullview.maj_diam,
        Gpnfullview.min_diam,
        Gpnfullview.reftb_ang_diam
        )

df_meta = pd.DataFrame(list(query_meta.dicts()))

for index, row in df_images.iterrows():
    curr_id = row['idPNMain']

    # if curr_id not in [422]:
    #     continue

    print("Working on:{}".format(curr_id))


    image_file = row['fitsfile']

    image = "/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/forfitting/zero_{}".format(image_file)

    if not os.path.isfile(image):
        continue

    data_meta = df_meta.loc[df_meta['idPNMain'] == curr_id]
    name = data_meta.iloc[0]['name']

    lit_diam = float(data_meta.iloc[0]['maj_diam'])
    bmaj = float(row['bmaj'])
    bmin = float(row['bmin'])
    bpa = float(row['bpa'])
    # fit_start = float(row['fit_start'])
    fit_start = 1
    if row['fit_start'] is not None and not np.isnan(row['fit_start']):
        fit_start = float(row['fit_start'])
    print('fit start:{}'.format(fit_start))

    RA = float(row['off_RA'])
    DEC = float(row['off_DEC'])

    tempimage = "fit_temp.fits"

    res_montage = ibmont.FITScutout(image,
                                    tempimage,
                                    RA,
                                    DEC,
                                    2 * lit_diam,
                                    2 * lit_diam
                                    )
    if not res_montage:
        ibfits.FITScutout_iraf(image,
                               tempimage,
                               RA,
                               DEC,
                               2 * lit_diam,
                               2 * lit_diam
                               )


    # xpix, ypix = ibfits.coord2pix(tempimage, RA, DEC)

    # init_sma = coordlen2pixlen(tempimage,lit_diam,45)
    init_sma = coordlen2pixlen(tempimage,fit_start,45)
    radius_pixels = coordlen2pixlen(tempimage,lit_diam,0)
    eps = 0.2

    pa = 45. / 180. * np.pi

# image = 'ellipse/test.fits'
    hdu = fits.open(tempimage)
    image_data = hdu[0].data
    hdu.close()

    # xpix, ypix = 10,10#int(image_data.shape[0]/2), int(image_data.shape[1]/2)
    xpix, ypix = int(image_data.shape[0]/2) + 1, int(image_data.shape[1]/2) + 1

    # image_data = data[0, 0, :, :]

    # TODO
    # find proper max
    # peak_image = np.max(image_data, where=~np.isnan(image_data), initial=-1)
    image_stats = ibfits.imageStats(tempimage, 5)
    peak_image = image_stats['perc99.5']

    isolevel = peak_image / 10.

    flag_pass = False
    curr_sma = init_sma

    while not flag_pass and curr_sma < radius_pixels:
        print(xpix, ypix, curr_sma, eps, pa, radius_pixels)
        init_geom = EllipseGeometry(xpix, ypix, curr_sma, eps, pa)
        ellipse = Ellipse(image_data, geometry=init_geom)
        try:
            isophote = ellipse.fit_image(maxit=50, minsma=init_sma, fix_center = True)

            df_iso = isophote.to_table().to_pandas()
            df_res = df_iso.iloc[(df_iso['intens'] - isolevel).abs().argsort()[:1]]
            sma_pix = float(df_res.iloc[0]['sma'])
            pa_fit = float(df_res.iloc[0]['pa'])
            ellipticity = float(df_res.iloc[0]['ellipticity'])
            pa_fit = float(df_res['pa'])
            ellipticity = float(df_res.iloc[0]['ellipticity'])
            df_res['maj_diam'] = pixlen2coordlen(tempimage, sma_pix, pa_fit) * 2
            df_res['min_diam'] = df_res['maj_diam'] * np.sqrt(1 - ellipticity ** 2)
            df_res['bmaj'] = bmaj * 3600
            df_res['bmin'] = bmin * 3600
            df_res['bpa'] = bpa
            df_res['idPNMain'] = curr_id
            df_res['fitsfile'] = image_file
            df_res['peak'] = peak_image
            df_res['band'] = row['band']
            df_res['obsyear'] = ibfits.getHeaderItems(tempimage, ['DATE-OBS'])['DATE-OBS']

            flag_pass = True

        except:
            print('fit failed')
            curr_sma = curr_sma + 0.5

    if flag_pass:
        list_of_dicts = df_res.to_dict(orient='records')
        FitRadioDiams.insert_many(list_of_dicts).execute()

        majdiam = float(df_res.iloc[0]['maj_diam'])
        mindiam = float(df_res.iloc[0]['min_diam'])
        radiopa = float(df_res.iloc[0]['pa'])

        # res_montage = ibmont.FITScutout(image,
        #                                 tempimage,
        #                                 RA,
        #                                 DEC,
        #                                 2 * majdiam/3600.,
        #                                 2 * majdiam/3600.
        #                                 )

        marker, figure = diam_image(inputFile=image,
                                    inRA=RA,
                                    inDEC=DEC,
                                    # percmin=5,
                                    # percmax=99,
                                    valmin=-image_stats['bkgrms'],
                                    valmax=image_stats['perc90'],
                                    imsize=majdiam/3600.,
                                    add_text="{}\n#PN{}; {}".format(name, curr_id, row['band']),
                                    addBeam=True
                                    )

        # 10% of the peak value

        marker.show_contour(image,
                            levels=[isolevel],
                            colors="blue",
                            convention='calabretta',
                            linestyles="-",
                            linewidths=1)

        # # rms contour levels
        marker.show_contour(image,
                            levels=[peak_image],
                            colors="yellow",
                            convention='calabretta',
                            linestyles="dotted",
                            linewidths=1)

        use= majdiam/(2*3600.)
        # marker.show_circles(RA, DEC, use)

        marker.show_ellipses(RA, DEC, majdiam/3600., mindiam/3600.,
                             radiopa,
                             edgecolor='red',
                             linestyles='dashed')


        marker.save("/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/forfitting/{}".format(image_file.replace(".fits","_diamfit.png")), dpi=150)
        plt.close(figure)


