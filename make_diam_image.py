import peeweedbmodels.dbSED_PNe as sed_pne
# import ibplotting.plotlibs as ibplot
import ibastro.fitslibs as ibastr
import ibastro.montages as ibmont
import shutil
import numpy as np
import os
from ibplotting import PlotObject
from importlib import reload

reload(PlotObject)


# curr_id = 730
#
# data_meta = pd.DataFrame({
#     "png"           : ["197.8+17.3"],
#     "idPNMain"      : [730],
#     "draj2000"      : [112.29487],
#     "ddecj2000"     : [20.91179],
#     "p_nstat"       : ["T"],
#     "name"          : ["NGC 2392"],
#     "maj_diam"      : [46.0],
#     "min_diam"      : [44.0],
#     "reftb_ang_diam": ["2016MNRAS.455.1459F"]}
#         )
#
# all_res = [
#     {
#         'model'       : 'cylindrical',
#         'mu'          : 0.4,
#         'theta'       : 31.59467772873837,
#     ]
#


def make_diam_image(curr_id, data_meta, df_results, radio_diams):
    min_cutuouts = {
        'hst'      : 3,
        'iphas'    : 30,
        'nvss'     : 150,
        'panstarrs': 15,
        'shs'      : 30,
        'sss'      : 30,
        'skymapper': 15
        }

    path_to_images = "/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/diam_images/"
    path_to_fits = "/Users/ibojicic/data/fitsImages/PN_images/"

    tableimages = sed_pne.DiamImages

    plot_models = ['sphshell']

    config_plots = {
        'literature'   : {
            'edgecolor': 'blue',
            'linestyle': ':',
            'linewidth': 1.5},
        'radio'        : {
            'edgecolor': 'green',
            'linestyle': 'dashdot',
            'linewidth': 1.5},
        'cylindrical'  : {
            'edgecolor': 'yellow',
            'linestyle': '--',
            'linewidth': 1.5},
        'trpowerlawmod': {
            'edgecolor': 'orange',
            'linestyle': 'dashed',
            'linewidth': 1.5},
        'sphshell'     : {
            'edgecolor': 'red',
            'linestyle': 'solid',
            'linewidth': 1.5}
        }

    alldiams = []

    print("working on {}".format(curr_id))

    radio_zoom = 0

    optdiam = None
    name = data_meta.iloc[0]['name']
    if data_meta.iloc[0]['maj_diam'] is not None:
        optdiam = float(data_meta.iloc[0]['maj_diam']) / 3600.
        alldiams.append(float(data_meta.iloc[0]['maj_diam']))

    radiodiam = None
    if radio_diams.iloc[0]['DiamRad'] is not None:
        radiodiam = float(radio_diams.iloc[0]['DiamRad']) / 3600.
        alldiams.append(float(radio_diams.iloc[0]['DiamRad']))

    # radiodiam = None
    # if data_meta.iloc[0]['DiamRad'] is not None:
    #     radiodiam = float(data_meta.iloc[0]['DiamRad']) / 3600.
    #     alldiams.append(float(data_meta.iloc[0]['DiamRad']))

    plotdiams = {
        'literature'   : optdiam,
        'radio'        : radiodiam,
        'cylindrical'  : None,
        'trpowerlawmod': None,
        'sphshell'     : None
        }

    if df_results.theta > radio_zoom:
        radio_zoom = df_results.theta

    plotdiams[df_results.model] = df_results.theta / 3600.

    alldiams.append(df_results.theta)

    imdata = tableimages.select().where(tableimages.idPNMain == curr_id,
                                        tableimages.use == 'y')

    if imdata.exists():

        for image in imdata:

            infile = "{}{}".format(path_to_fits, image.fitsfile)

            ibastr.add_beam_pars(infile)

            bmajval = ibastr.getHeaderItems(infile, ['BMAJ'])
            if bmajval['BMAJ'] is not None:
                alldiams.append(bmajval['BMAJ'] * 3600 * 5)

            zoom_factor = 1.2
            maxdiam = np.nanmax(alldiams)
            if radio_zoom > maxdiam * 0.7:
                zoom_factor = 1.5

            if image.instrument in min_cutuouts and maxdiam < min_cutuouts[image.instrument]:
                maxdiam = min_cutuouts[image.instrument]

            currimsize = zoom_factor * maxdiam / (image.imsize_factor * 3600.)

            if image.off_RA is not None:
                RA = float(image.off_RA)
                DEC = float(image.off_DEC)
            else:
                RA = float(image.DRAJ2000)
                DEC = float(image.DDECJ2000)

            newimage = PlotObject.PlotObject(infile, RA, DEC, working_dir=path_to_images)
            newimage.north = True
            newimage.obj_diam = optdiam
            image_stats = newimage.image_stats(RA, DEC, maxdiam, maxdiam)

            if image.instrument == 'vla' or image.instrument == 'nvss':
                cont_levels = np.array([0.1]) * image_stats['max']
                rms_levels = np.array([3, 5, 8, 12, 17, 23]) * image_stats['bkgrms']
            else:
                cont_levels = None
                rms_levels = None

            newimage.range_val = [image_stats['perc10'], image_stats['perc99']]
            newimage.imsize = currimsize
            newimage.center_image(RA, DEC)
            newimage.bw_image(stretch_scale='arcsinh')

            newimage.add_contours(cont_levels, colors="cyan", convention='calabretta', linestyles="-", linewidths=1.2)
            newimage.add_contours(rms_levels, colors="white", convention='calabretta', linestyles="-", linewidths=0.7)
            for model, cnfg in config_plots.items():
                if plotdiams[model] is None:
                    continue
                newimage.add_ellipse(RA, DEC, plotdiams[model], **cnfg)
            newimage.add_text_bar("{}; {}; {}".format(name, image.instrument, image.band))
            newimage.add_scalebar()
            newimage.add_beam()
            newimage.hide_labels(True)
            pngfile = "{}{}_{}_{}.png".format(path_to_images, image.idPNMain, image.band, image.instrument)
            newimage.save_image(pngfile)
