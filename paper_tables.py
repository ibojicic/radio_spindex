# from load_data import *
from importlib import reload
import load_data as ld
import pandas as pd
import numpy as np
import sys
import sigfig
from astropy.coordinates import ICRS
from astropy import units as u

reload(ld)

pathtopaper = "/Users/ibojicic/data/papers/SEDPNe_new/"
hstfitteddiams = "{}tables/hstfitteddiams.tex".format(pathtopaper)
hstfitteddiams_short = "{}tables/hstfitteddiams_short.tex".format(pathtopaper)
vlafitteddiams = "{}tables/vlafitteddiams.tex".format(pathtopaper)
vlafitteddiams_short = "{}tables/vlafitteddiams_short.tex".format(pathtopaper)
hstfittedimages = "{}files/hstimages".format(pathtopaper)
vlafittedimages = "{}files/vlaimages".format(pathtopaper)

sedsphshell = "{}tables/sedsphshell.tex".format(pathtopaper)
sedsphshell_short = "{}tables/sedsphshell_short.tex".format(pathtopaper)


def toint(x):
    return int(x)


def f0(x):
    return "{:.0f}".format(x)


def f1(x):
    return "{:.1f}".format(x)


def pafit(x):
    return "{:.0f}".format(x - 90)


def round_to_unc(row, val, unc):
    rounded = sigfig.round(row[val], row[unc], sep=list, type=float)
    row[[val, unc]] = rounded
    return row


def round_to_sigfig(row, val, sigfigs):
    rounded = sigfig.round(row[val], sigfigs=sigfigs)
    row[val] = rounded
    return row


def latex_valunc(row, val, unc, newcol):
    rounded = sigfig.round(row[val], row[unc])
    row[newcol] = rounded.replace("±", "$\pm$")
    return row


def format_coords(row, ra, dec):
    c = ICRS(row[ra] * u.degree, row[dec] * u.degree)
    ra_formatted = c.ra.to_string(u.hour, alwayssign=True, sep=':', precision=2)
    dec_formatted = c.dec.to_string(u.degree, alwayssign=True, sep=':', precision=1)
    row[[ra, dec]] = [ra_formatted, dec_formatted]
    return row


## copy dfs that you need
# df_fitted_diams_merged = ld.df_fitted_diams_merged.copy()
df_vla_fitted_diams = ld.df_vla_fitted_diams.copy()
# df_fitted_diams_merged = ld.df_radio_diams_ivan.copy()
df_fitted_diams_hst = ld.df_hst_diams.copy()
df_meta_all = ld.df_meta_all.copy()
df_full_data = ld.df_full_data.copy()

#######################################################
# newly measured VLA diams
#######################################################

df_vla_fitted_diams.rename(columns={'maj_diam': 'majdiam', 'min_diam': 'mindiam'}, inplace=True)

df_vla_fitted_diams['beam'] = round(df_vla_fitted_diams['bmaj'], 2).astype(str) + \
                              "$\\times$" + round(df_vla_fitted_diams['bmin'], 2).astype(str)

df_vla_fitted_diams['peak'] = df_vla_fitted_diams['peak'] * 1e3
df_vla_fitted_diams['intens'] = df_vla_fitted_diams['intens'] * 1e3

df_vla_fitted_diams.set_index(['idPNMain', 'png', 'band'], inplace=True)

select_columns = {
    'majdiam': '$\\theta_{maj}$',
    'mindiam': '$\\theta_{min}$',
    'pa'     : 'PA$_{\\theta}$',
    'beam'   : 'beam',
    'bpa'    : 'PA$_{beam}$',
    'peak'   : 'Peak',
    'intens' : '10\%',
    'obsyear': 'ObsDate'
}
formater = {
    # 'idPNMain'  : '\#PN',
    # 'png'       : 'PNG',
    'majdiam': f1,
    'mindiam': f1,
    'pa'     : pafit,
    # 'beam'   : 'beam',
    'bpa'    : f0
    # 'band'      : 'band',
    # 'obsyear': 'ObsDate'
}

caption_newdiams = "Newly measured angular diameters from NVAS images. The columns are as follows: " \
                   "(1) HASH PN id ; (2) PNG designation as defined in \citep{Acker1992a}; (3) radio band " \
                   "of the source image; (4), (5) and (6) estimated major axis, minor axis and positional " \
                   "angle, respectively; (7) and (8) beam parameters in the source image; (9) intensity " \
                   "level at 99.5 percentile, (10) intensity level at fitted angular diameter and (11) " \
                   "the date when the observation was made."

# headers
# (1) & (2) & (3) & (4) & (5) & (6) & (7) & (8) & (9) & (10) & (11) \\
# \#PN & PNG & band  & $\theta_{maj}$ & $\theta_{min}$ & PA$_{\theta}$ & $\theta_{beam}$ & PA$_{beam}$ & Peak &  10\% & ObsDate \\
#  &  &  & ["] & ["] & [$^{\circ}$] & ["$\times$"] & [$^{\circ}$] & mJy/beam  & mJy/beam & \\

latex_vla_fitted_diams = df_vla_fitted_diams.to_latex(columns=select_columns.keys(), header=select_columns.values(),
                                                      index=True, na_rep='...', float_format="%.2f", index_names=True,
                                                      caption=caption_newdiams, label="tab:fitteddiams",
                                                      formatters=formater, longtable=True, escape=False, multirow=True,
                                                      sparsify=True, column_format='rrrrrrcrrrr')
with open(vlafitteddiams, "w") as f:
    f.write(latex_vla_fitted_diams)

latex_vla_fitted_diams_short = df_vla_fitted_diams.sort_values(by='idPNMain').head(10). \
    to_latex(columns=select_columns.keys(), header=select_columns.values(), index=True, na_rep='...',
             float_format="%.2f", index_names=True, caption=caption_newdiams, label="tab:vlafitteddiams_short",
             formatters=formater, longtable=False, escape=False, multirow=True, sparsify=True,
             column_format='rrrrrrcrrrr')

with open(vlafitteddiams_short, "w") as f:
    f.write(latex_vla_fitted_diams_short)

#######################################################
# newly measured HST diams
#######################################################

df_fitted_diams_hst.rename(columns={'maj_diam': 'majdiam', 'min_diam': 'mindiam'}, inplace=True)

df_fitted_diams_hst = pd.merge(df_fitted_diams_hst, df_meta_all, on='idPNMain', how='left')

df_fitted_diams_hst.set_index(['idPNMain'], inplace=True)

select_columns = {
    # 'idPNMain'  : 'idPNMain',
    'png'    : 'png',
    'majdiam': '$\\theta_{maj}$',
    'mindiam': '$\\theta_{min}$',
    'pa'     : 'PA$_{\\theta}$',
    'peak'   : 'peak',
    'intens' : 'intens',
    'band'   : 'band'
}
formater = {
    # 'idPNMain'  : '\#PN',
    # 'png'       : 'PNG',
    'majdiam': f1,
    'mindiam': f1,
    'pa'     : pafit
    # 'peak':f1,
    # 'intens':f1
}

caption_newdiams = "Newly measured angular diameters from HST images. The columns are as follows: " \
                   "(1) HASH PN id ; (2) PNG designation as defined in \citep{Acker1992a}; (3), (4) and " \
                   "(5) estimated major axis, minor axis and positional angle, respectively; (6) intensity " \
                   "level at 99.5 percentile, (7) intensity level at fitted angular diameter, and (8) HST filter."

# headers
# (1) & (2) & (3) & (4) & (5) & (6) & (7) & (8) \\
# \#PN & PNG & $\theta_{maj}$ & $\theta_{min}$ & PA$_{\theta}$ & Peak & 10\% &   band \\
#  & & ["] & ["] & [$^{\circ}$] & counts/s & counts/s  &        \\


latex_hst_fitted_diams = df_fitted_diams_hst.to_latex(columns=select_columns.keys(), header=select_columns.values(),
                                                      index=True, na_rep='...', float_format="%.2f", index_names=True,
                                                      caption=caption_newdiams, label="tab:hstfitteddiams",
                                                      formatters=formater, longtable=False, escape=False,
                                                      multirow=False,
                                                      sparsify=True, column_format='rrrrrrrr')
with open(hstfitteddiams, "w") as f:
    f.write(latex_hst_fitted_diams)

latex_hst_fitted_diams = df_fitted_diams_hst.sort_values(by='idPNMain').head(10). \
    to_latex(columns=select_columns.keys(), header=select_columns.values(), index=True, na_rep='...',
             float_format="%.2f", index_names=True, caption=caption_newdiams, label="tab:hstfitteddiams_short",
             formatters=formater, longtable=False, escape=False, multirow=False, sparsify=True,
             column_format='rrrrrrrr')

with open(hstfitteddiams_short, "w") as f:
    f.write(latex_hst_fitted_diams)

#######################################################
# SED results
#######################################################

main_cols = ['idPNMain', 'png', 'draj2000', 'ddecj2000',  'theta_x', 'theta_err_x', 'freq_0_x', 'freq_0_err_x', 'EM_x',
             'EM_err_x', 'n_points_x', 'nrmse_x', 'qsmape_x']

df_sed_spshell = df_full_data[~df_full_data.theta_x.isna()][main_cols]

# add value \pm unc column for latex
df_sed_spshell = df_sed_spshell.apply(lambda row: latex_valunc(row, 'theta_x', 'theta_err_x', 'theta'), axis=1)
df_sed_spshell = df_sed_spshell.apply(lambda row: latex_valunc(row, 'freq_0_x', 'freq_0_err_x', 'freq_0'), axis=1)
df_sed_spshell = df_sed_spshell.apply(lambda row: latex_valunc(row, 'EM_x', 'EM_err_x', 'EM'), axis=1)

# format coordinates
df_sed_spshell = df_sed_spshell.apply(lambda row: format_coords(row, 'draj2000', 'ddecj2000'), axis=1)
df_sed_spshell['png'] = df_sed_spshell['png'].str.replace('-','--')

# format columns for electronic table
df_sed_spshell = df_sed_spshell.apply(lambda row: round_to_unc(row, 'theta_x', 'theta_err_x'), axis=1)
df_sed_spshell = df_sed_spshell.apply(lambda row: round_to_unc(row, 'freq_0_x', 'freq_0_err_x'), axis=1)
df_sed_spshell = df_sed_spshell.apply(lambda row: round_to_unc(row, 'EM_x', 'EM_err_x'), axis=1)

df_sed_spshell = df_sed_spshell.apply(lambda row: round_to_sigfig(row, 'nrmse_x', 2), axis=1)

# df_sed_spshell.set_index(['idPNMain'], inplace=True)

select_columns = {
    'idPNMain'  : '\#PN',
    'png': 'PNG',
    'draj2000'  : 'RA J2000',
    'ddecj2000' : 'DEC J2000',
    'theta'     : '$\\theta_{SED}$',
    'freq_0'    : '\\nucr\\',
    'EM'        : 'EM',
    'n_points_x': 'n$_{points}$',
    'nrmse_x'   : '$\mathsf{nrmse}$',
    'qsmape_x'  : '$Q_{tot}$'
}
formater = {
    'n_points_x': f0,
    'qsmape_x'  : f0
}

caption_sed = "Excerpt of the table of newly estimated angular diameters from " \
              "SED modelling. The full table as provided in electronic format. " \
              "The columns are as follows: (1) HASH PN id; (2) PNG designation; " \
              "(3) and (4) centroid position of the nebula from HASH PN catalogue; " \
              "(5) SED estimated angular diameter; (6) SED estimated critical frequency; " \
              "(7) SED estimated emission measure; (8) number of data points (radio fluxes) " \
              "used in SED fitting; (9) $\mathsf{nrmse}$ value; (10) total fit quality " \
              "(as defined in the text)."

# headers
# (1) & (2) & (3) & (4) & (5) & (6) & (7) & (8) & (9) & (10) \\
# \#PN &  PNG & RA J2000 & DEC J2000 & $\theta_{SED}$ & $\nu_{c}$ & EM & n$_{points}$ & $\mathsf{nrmse}$ & $Q_{tot}$ \\
#  & & & & ["] & [GHz] & [pc cm$^{-6}$] & & [\%] & \\


latex_sed_sphshell = df_sed_spshell.to_latex(columns=select_columns.keys(), header=select_columns.values(),
                                             index=False, na_rep='...', index_names=True,
                                             caption=caption_newdiams, label="tab:sedsphshell",
                                             formatters=formater, longtable=False, escape=False,
                                             multirow=False,
                                             sparsify=True, column_format='rrrrrrrrrr')
with open(sedsphshell, "w") as f:
    f.write(latex_sed_sphshell)

latex_sed_sphshell_short = df_sed_spshell.sort_values(by='idPNMain').head(10). \
    to_latex(columns=select_columns.keys(), header=select_columns.values(), index=False, na_rep='...',
             index_names=True, caption=caption_newdiams, label="tab:sedsphshell_short",
             formatters=formater, longtable=False, escape=False, multirow=False, sparsify=True,
             column_format='rrrrrrrrrr')

with open(sedsphshell_short, "w") as f:
    f.write(latex_sed_sphshell_short)

#######################################################
# images of newly measured HST diams
#######################################################
df_fitted_diams_hst.reset_index(inplace=True)

no_per_page = 20

no_figs = int(np.ceil(len(df_fitted_diams_hst) / no_per_page))

with open("{}.tex".format(hstfittedimages), "w") as f:
    for figure_no in range(no_figs):
        gr_start = figure_no * no_per_page
        gr_end = gr_start + no_per_page

        df_group = df_fitted_diams_hst.iloc[gr_start:gr_end]

        fulline = ""

        for id, row in df_group.iterrows():
            filename = "{}_{}_hst_diamfit.png".format(row['idPNMain'], row['band'])
            imgline = "\\includegraphics[width=0.5\\columnwidth]{{\\hstimages/{}}}\n".format(filename)
            fulline = fulline + imgline

        f.write("\\begin{figure*}\n")
        f.write(fulline)
        f.write("\\caption{{Some caption}}\n\\label{{fig:hstimages{}}}\\end{{figure*}}\n\n".format(figure_no))

#######################################################
# images of newly measured VLA diams
#######################################################

df_vla_fitted_diams.reset_index(inplace=True)

no_per_page = 20

no_figs = int(np.ceil(len(df_vla_fitted_diams) / no_per_page))

with open("{}.tex".format(vlafittedimages), "w") as f:
    for figure_no in range(no_figs):
        gr_start = figure_no * no_per_page
        gr_end = gr_start + no_per_page

        df_group = df_vla_fitted_diams.iloc[gr_start:gr_end]

        fulline = ""

        for id, row in df_group.iterrows():
            filename = "{}_{}_vla_diamfit.png".format(row['idPNMain'], row['band'])
            imgline = "\\includegraphics[width=0.5\\columnwidth]{{\\vlaimages/{}}}\n".format(filename)
            fulline = fulline + imgline

        f.write("\\begin{figure*}\n")
        f.write(fulline)
        f.write("\\caption{{Some caption}}\n\\label{{fig:vlaimages{}}}\\end{{figure*}}\n\n".format(figure_no))
