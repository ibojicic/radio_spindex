import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import pandas as pd
import numpy as np
from importlib import reload
from ibplotting import PlotFacetSed
import sys

model = reload(model)
PlotFacetSed = reload(PlotFacetSed)

no_rows = 7
no_cols = 4

per_page = no_cols * no_rows

path = "/Users/ibojicic/data/papers/SEDPNe_new/"

models_config = {
    'sphshell'     : {
        'linetype': '-',
        'color'   : '#990000',
    },
    'trpowerlawmod': {
        'linetype': 'dashdot',
        'color'   : '#006600',
    },
    # 'cylindrical'  : {
    #     'linetype': '--',
    #     'color'   : '#000066',
    # },
    'powerlaw'     : {
        'linetype': 'dotted',
        'color'   : '#000066'
    }

}

query_select = Gpnfullview.select(
    Gpnfullview.idPNMain,
    Gpnfullview.PNstat,
    Gpnfullview.domain
).where((Gpnfullview.PNstat == 'T') & (Gpnfullview.domain == 'Galaxy'))

df_select = pd.DataFrame(list(query_select.dicts()))

tbl_results = model.PneFitResults
query_results = tbl_results.select().where(tbl_results.idPNMain << list(df_select.idPNMain))
df_results_all = pd.DataFrame(list(query_results.dicts()))

df_results = df_results_all[df_results_all.fittype != 'curvefit'].copy()
all_ids = df_results.idPNMain.unique()

tbl_radio_data = model.RadioData
query_radio = tbl_radio_data.select().where((tbl_radio_data.InUse == 'y') & (tbl_radio_data.idPNMain << list(all_ids)))
df_radio = pd.DataFrame(list(query_radio.dicts()))
df_radio.flux_err.fillna(df_radio.flux / 10., inplace=True)

df_radio['flux_min'] = df_radio['flux'] - df_radio['flux_err']
df_radio.loc[df_radio['flux_min'] < 0, 'flux_min'] = 1.e-3
df_radio['flux_max'] = df_radio['flux'] + df_radio['flux_err']

sorted_by_flux = np.array(
    df_radio[['idPNMain', 'flux']].groupby('idPNMain').mean().sort_values('flux').reset_index().idPNMain)

n = len(sorted_by_flux)

rem = n % per_page

if rem != 0:
    sorted_by_flux = np.pad(sorted_by_flux, (0, per_page - rem), 'constant')

groups = sorted_by_flux.reshape([-1, per_page])

query_meta = Gpnfullview.select(
    Gpnfullview.png,
    Gpnfullview.idPNMain,
    Gpnfullview.draj2000,
    Gpnfullview.ddecj2000,
    Gpnfullview.PNstat,
    Gpnfullview.name,
    Gpnfullview.maj_diam,
    Gpnfullview.min_diam,
    Gpnfullview.reftb_ang_diam
).where(Gpnfullview.idPNMain << list(all_ids))

df_meta = pd.DataFrame(list(query_meta.dicts()))

group_id = 0

latex_appendix_file = open("{}files/Appendix1.tex".format(path), 'w')

for page in groups:
    print(page)
    group_id += 1
    df_radio_group = df_radio[df_radio.idPNMain.isin(page)]
    df_results_group = df_results[df_results.idPNMain.isin(page)]

    newplt = PlotFacetSed.PlotFacetSed()
    newplt.base_data = df_radio_group
    newplt.facet_SEDs(df_results_group, models_config, nrocol=no_cols)
    newplt = newplt.save_plot(newplt.plot_out(), "{}sed_groups/group_{}.png".format(path, group_id),
                              heightim=0.85 * 29.7, widthim=21., units='cm', )

    latex_string = "\\begin{{figure*}}\n" \
                   "\\includegraphics[width=\\textwidth]{{\\pathsedgroups/group_{}.png}}\n" \
                   "\\caption{{SED fit results for (from top left to bottom right) \\#PNe={} }}\n" \
                   "\\label{{fig:gxmodels}}\n" \
                   "\\end{{figure*}}\n\n".format(group_id, ", ".join(list(page.astype('str'))))

    latex_appendix_file.write(latex_string)

latex_appendix_file.close()
