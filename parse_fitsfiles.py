import peeweedbmodels.dbSED_PNe as sed_pne
import ibastro.fitslibs as ibastr
import glob, os

tableout = sed_pne.DiamImages

fitsfolder = "/Users/ibojicic/data/fitsImages/PN_images/"

k = 0
for file_path in glob.glob("{}*.fits".format(fitsfolder)):
    k = k + 1
    print(k)
    file = os.path.basename(file_path)
    chunks1 = file.split(".")
    chunks2 = chunks1[0].split('_')
    check_image = tableout.select().where(tableout.fitsfile == file)
    if not check_image.exists():
        year_obs = ibastr.getHeaderItems(file_path, ['DATE-OBS'])

        iminput = {
            'idPNMain'  : int(chunks2[0]),
            'fitsfile'  : file,
            'band'      : chunks2[1],
            'instrument': chunks2[2],
            'dateobs'   : year_obs['DATE-OBS']

        }
        if chunks2[2] == 'vla':
            beam_pars = ibastr.get_vla_beam_pars(file_path)
            iminput['bmaj'] = beam_pars['BMAJ']
            iminput['bmin'] = beam_pars['BMIN']
            iminput['bpa'] = beam_pars['BPA']
        print(iminput)
        tableout.create(**iminput)
    else:
        print(file)
