import sys

import pandas as pd
import numpy as np
from peeweedbmodels.dbMainGPN import Gpnfullview
from peeweedbmodels.dbSED_PNe import RadioData, FitRadioDiams, PneFitResults, PneFitResults_limits, \
    RadioDiamData, Frew_Distances, manual_fit_quality, FitRadioDiams_hst
from peeweedbmodels.dbMainPNData import Frew2016TblA4, Tylenda2003Tbl2, Ruffle2004Tbl4
from ibastro import physics as ibph
from uncertainties import unumpy

# selection parameters
select_pnstat = ['T']
select_max_diam = 50
select_nu0_factor = 1.5  # exclude if no points below factor * nu0
select_limit_fit_quality = 4  # select only fits with quality below

all_cols = ['Te', 'freq_0', 'freq_0_start', 'freq_0_fixed', 'freq_0_err', 'idPNMain', 'intersect',
            'intersect_err', 'power', 'power_err', 'model', 'fittype', 'mse', 'n_points', 'mu', 'nrmse',
            'mape', 'smape', 'redchisqg', 'theta', 'theta_err', 'theta_start', 'theta_fixed', 'time_in']


def unique_bands(df):
    return df.band.nunique() > 2


def noqual(df, radio_data, freq_0, qsmape):
    rd = radio_data[radio_data.idPNMain == df.idPNMain]
    no_thick = rd[rd.freq <= select_nu0_factor * df[freq_0]].shape[0]
    no_thin = rd[rd.freq > select_nu0_factor * df[freq_0]].shape[0]
    quality = df[qsmape] * 3
    if no_thick > 1 and no_thin > 1:
        quality = df[qsmape]
    elif no_thick > 1 and no_thin == 1:
        quality = df[qsmape] * 2
    return quality


#################################################
# GPNe Full View
#################################################

query_meta = Gpnfullview.select(
    Gpnfullview.png,
    Gpnfullview.idPNMain,
    Gpnfullview.raj2000,
    Gpnfullview.decj2000,
    Gpnfullview.draj2000,
    Gpnfullview.ddecj2000,
    Gpnfullview.glon,
    Gpnfullview.glat,
    Gpnfullview.PNstat,
    Gpnfullview.name,
    Gpnfullview.maj_diam,
    Gpnfullview.min_diam,
    Gpnfullview.reftb_ang_diam,
    Gpnfullview.main_class
).where(Gpnfullview.PNstat << select_pnstat, Gpnfullview.domain == 'Galaxy')
df_meta_all = pd.DataFrame(list(query_meta.dicts()))

df_meta_limited = df_meta_all[df_meta_all.maj_diam < select_max_diam].copy()

#################################################
# Define opt diam dataframe
#################################################

df_opt_diams = pd.DataFrame({'DiamOpt': [], 'idPNMain': [], 'ref_optdiam': []})

#################################################
# 10% diams from HST images measured by me
#################################################

query_hst_diams = FitRadioDiams_hst.select().where(FitRadioDiams_hst.idPNMain << df_meta_all.idPNMain.values.tolist(),
                                                   FitRadioDiams_hst.use == 'y')
df_hst_diams = pd.DataFrame(list(query_hst_diams.dicts()))

df_hst_diams['DiamOpt'] = np.round(
    np.sqrt(df_hst_diams['maj_diam'].astype('float') * df_hst_diams['min_diam'].astype('float')),
    2)

df_hst_diams['ref_optdiam'] = 'this paper'

# add to main df

df_opt_diams = pd.concat([df_opt_diams, df_hst_diams[['DiamOpt', 'idPNMain', 'ref_optdiam']]])

#################################################
# Ruffle2004Tbl4 best diameters
#################################################

query_ruffle_diams = Ruffle2004Tbl4.select(Ruffle2004Tbl4.idPNMain,
                                           Ruffle2004Tbl4.theta_maj,
                                           Ruffle2004Tbl4.theta_min,
                                           Ruffle2004Tbl4.theta_mean_err).where(
    Ruffle2004Tbl4.line == 'Ha',
    Ruffle2004Tbl4.idPNMain << df_meta_all.idPNMain.values.tolist()
    # ~Ruffle2004Tbl4.idPNMain << df_hst_diams.idPNMain.values.tolist()
).group_by(Ruffle2004Tbl4.idPNMain)

df_ruffle_diams = pd.DataFrame(list(query_ruffle_diams.dicts()))

df_ruffle_diams.loc[df_ruffle_diams.theta_min.isna(), 'theta_min'] = df_ruffle_diams.loc[
    df_ruffle_diams.theta_min.isna(), 'theta_maj']

df_ruffle_diams['DiamOpt'] = np.round(
    np.sqrt(df_ruffle_diams['theta_maj'].astype('float') * df_ruffle_diams['theta_min'].astype('float')),
    2)

df_ruffle_diams['ref_optdiam'] = 'Ruffle2004'

# select extra diams (not in previous data)
df_ruffle_diams_extra = df_ruffle_diams[~df_ruffle_diams.idPNMain.isin(df_opt_diams.idPNMain)]

# add to main df
df_opt_diams = pd.concat([df_opt_diams, df_ruffle_diams_extra[['DiamOpt', 'idPNMain', 'ref_optdiam']]])

# comparison my hst and ruffle
df_hst_vs_ruffle = pd.merge(df_hst_diams, df_ruffle_diams, on='idPNMain', how='inner')

#################################################
# Tylenda 2003 Halpha best diameters (table2)
#################################################

query_tylenda_diams = Tylenda2003Tbl2.select(Tylenda2003Tbl2.idPNMain,
                                             Tylenda2003Tbl2.diam,
                                             Tylenda2003Tbl2.diamb).where(
    Tylenda2003Tbl2.idPNMain << df_meta_all.idPNMain.values.tolist()
    # ~Tylenda2003Tbl2.idPNMain << df_hst_diams.idPNMain.values.tolist(),
    # ~Tylenda2003Tbl2.idPNMain << df_ruffle_diams.idPNMain.values.tolist()
)

df_tylenda_diams = pd.DataFrame(list(query_tylenda_diams.dicts()))

df_tylenda_diams.loc[df_tylenda_diams.diamb.isna(), 'diamb'] = df_tylenda_diams.loc[
    df_tylenda_diams.diamb.isna(), 'diam']

df_tylenda_diams['DiamOpt'] = np.round(
    np.sqrt(df_tylenda_diams['diam'].astype('float') * df_tylenda_diams['diamb'].astype('float')),
    2)

df_tylenda_diams['ref_optdiam'] = 'Tylenda2003'

# select extra diams (not in previous data)
df_tylenda_diams_extra = df_tylenda_diams[~df_tylenda_diams.idPNMain.isin(df_opt_diams.idPNMain)]

# add to main df
df_opt_diams = pd.concat([df_opt_diams, df_tylenda_diams_extra[['DiamOpt', 'idPNMain', 'ref_optdiam']]])

# comparison my hst and tylenda
df_hst_vs_tylenda = pd.merge(df_hst_diams, df_tylenda_diams, on='idPNMain', how='inner')

#################################################
# 10% diams from vla images measured by me
#################################################

query_fitted_diams = FitRadioDiams.select().where(FitRadioDiams.idPNMain << df_meta_all.idPNMain.values.tolist(),
                                                  FitRadioDiams.use == 'y')
df_vla_fitted_diams = pd.DataFrame(list(query_fitted_diams.dicts()))

# add meta columns
df_vla_fitted_diams = pd.merge(df_vla_fitted_diams, df_meta_all[['idPNMain', 'png']],
                               on='idPNMain', how='left').sort_values(by='idPNMain')

df_vla_fitted_diams_avg = df_vla_fitted_diams[['idPNMain', 'maj_diam', 'min_diam']].groupby('idPNMain').agg(
    np.mean).reset_index()

df_vla_fitted_diams_avg['DiamRad'] = np.round(
    np.sqrt(df_vla_fitted_diams_avg['maj_diam'].astype('float') * df_vla_fitted_diams_avg['min_diam'].astype('float')),
    2)

df_vla_fitted_diams_avg['ref_radiodiam'] = 'this paper'
df_vla_fitted_diams_avg['method'] = '10perc'


#################################################
# radio diam data from literature
#################################################

def pick_diam(group):
    listback = ['maj_diam', 'min_diam', 'bibtex', 'method']

    if group.idPNMain.count() == 1:
        return group[listback]
    else:
        if '10perc' in list(group.method):
            return group.loc[group.method == '10perc'].iloc[[0]][listback]
        elif 'avgvis' in list(group.method):
            return group.loc[group.method == 'avgvis'].iloc[[0]][listback]
        elif 'deconv' in list(group.method):
            return group.loc[group.method == 'deconv'].iloc[[0]][listback]
    raise Exception("No method found")


# use only methods from '10perc', 'avgvis', 'deconv', 'em'
use_methods = ['10perc', 'avgvis', 'deconv']
query_radiodiamdata = RadioDiamData.select(RadioDiamData.idPNMain, RadioDiamData.maj_diam,
                                           RadioDiamData.min_diam, RadioDiamData.bibtex,
                                           RadioDiamData.method
                                           ).where(RadioDiamData.InUse == 'y',
                                                   RadioDiamData.method << use_methods,
                                                   RadioDiamData.flag_diam.is_null(True))
df_radio_lit_diams = pd.DataFrame(list(query_radiodiamdata.dicts()))

# if min_diam not available copy maj diam to min diam column
df_radio_lit_diams.loc[df_radio_lit_diams.min_diam.isna(), 'min_diam'] = df_radio_lit_diams.loc[
    df_radio_lit_diams.min_diam.isna(), 'maj_diam']

df_radiodiamdata_avg = df_radio_lit_diams.groupby('idPNMain').apply(lambda group: pick_diam(group)).\
    reset_index().drop(['level_1'], axis=1)

# DiamRad column is geometric average of maj diam and min diam (i.e. sqrt(maj_diam*min_diam)
df_radiodiamdata_avg['DiamRad'] = np.round(
    np.sqrt(df_radiodiamdata_avg['maj_diam'].astype('float') * df_radiodiamdata_avg['min_diam'].astype('float')),
    2)

df_radiodiamdata_avg.rename(columns={'bibtex': 'ref_radiodiam'}, inplace=True)

#################################################
# concat literature diams and my diams, use literature diams first
#################################################

# df_all_diams = pd.concat([df_vla_fitted_diams_avg,
#                           df_radiodiamdata_avg[~df_radiodiamdata_avg.idPNMain.isin(list(df_vla_fitted_diams_avg.idPNMain))]])
#
# df_all_diams = pd.merge(df_all_diams, df_opt_diams, on='idPNMain', how='outer')

df_all_diams = pd.concat([df_radiodiamdata_avg,
                          df_vla_fitted_diams_avg[~df_vla_fitted_diams_avg.idPNMain.isin(list(df_radiodiamdata_avg.idPNMain))]])

df_all_diams = pd.merge(df_all_diams, df_opt_diams, on='idPNMain', how='outer')

#################################################
# SED_PNe radio data
#################################################

query_radio_data = RadioData.select().where(RadioData.idPNMain << df_meta_limited.idPNMain.values.tolist(),
                                            RadioData.InUse == 'y')
df_radio_data = pd.DataFrame(list(query_radio_data.dicts()))

# count of all pne with at least one flux
total_radio_counts = df_radio_data.idPNMain.nunique()

# count of pne with radio data in at least 3 different bands
df_radio_data_used = df_radio_data.groupby('idPNMain').filter(unique_bands)
radio_counts = df_radio_data_used[['idPNMain', 'flux']].groupby(['idPNMain']).count()

# statistics on how many detections in different bands
radio_band_freqs = df_radio_data_used[['band', 'freq']].groupby(['band']).mean().reset_index()
radio_band_counts = df_radio_data_used[['band', 'flux']].groupby(['band']).count().reset_index()
radio_band_counts_merged = pd.merge(radio_band_counts, radio_band_freqs, on='band', how='left')

#################################################
# distances
#################################################

query_distances = Frew_Distances.select()
df_distances = pd.DataFrame(list(query_distances.dicts()))
df_distances['distmeth'] = df_distances.apply(lambda row: 'S' if row.meth == 'Sb-r' else 'P', axis=1)

#################################################
# manual fit quality
#################################################

query_manfit = manual_fit_quality.select()
df_manfit = pd.DataFrame(list(query_manfit.dicts()))

#################################################
# fit results
#################################################

select_fit = ['idPNMain', 'model', 'Te', 'mu', 'theta', 'theta_err', 'freq_0', 'freq_0_err', 'freq_0_start',
              'theta_start', 'mse', 'n_points', 'nrmse', 'mape', 'smape', 'redchisqg']

#################################################
# model = sphshell
#################################################

query_pnfit_results_sphshell = PneFitResults.select(). \
    where(PneFitResults.model == 'sphshell',
          PneFitResults.theta.is_null(False),
          PneFitResults.freq_0_start < PneFitResults.freq_0 * select_nu0_factor,
          PneFitResults.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_sphshell = pd.DataFrame(list(query_pnfit_results_sphshell.dicts()))[select_fit]

# EM and EM uncertanty for sphshell
EM_unc = unumpy.uarray(df_sphshell.freq_0, df_sphshell.freq_0_err)
EM = ibph.EM(EM_unc, 1.E4)/1E6
df_sphshell['EM'] = pd.Series(unumpy.nominal_values(EM))
df_sphshell['EM_err'] = pd.Series(unumpy.std_devs(EM))

#################################################
query_pnfit_results_sphshell_rejected = PneFitResults.select(). \
    where(PneFitResults.model == 'sphshell',
          PneFitResults.theta.is_null(False),
          PneFitResults.freq_0_start >= PneFitResults.freq_0 * select_nu0_factor,
          PneFitResults.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_sphshell_rejected = pd.DataFrame(list(query_pnfit_results_sphshell_rejected.dicts()))[select_fit]
# EM and EM uncertanty for sphshell
EM_unc = unumpy.uarray(df_sphshell_rejected.freq_0, df_sphshell_rejected.freq_0_err)
EM = ibph.EM(EM_unc, 1.E4)/1E6
df_sphshell_rejected['EM'] = pd.Series(unumpy.nominal_values(EM))
df_sphshell_rejected['EM_err'] = pd.Series(unumpy.std_devs(EM))

query_pnfit_results_fixed = PneFitResults_limits.select(). \
    where(PneFitResults_limits.model == 'sphshell',
          PneFitResults_limits.theta.is_null(False),
          PneFitResults_limits.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_sphshell_fixed = pd.DataFrame(list(query_pnfit_results_fixed.dicts()))[select_fit]

# EM and EM uncertanty for sphshell
EM_unc = unumpy.uarray(df_sphshell_fixed.freq_0, df_sphshell_fixed.freq_0 / 100)
EM = ibph.EM(EM_unc, 1.E4)/1E6
df_sphshell_fixed['EM'] = pd.Series(unumpy.nominal_values(EM))
df_sphshell_fixed['EM_err'] = pd.Series(unumpy.std_devs(EM))

df_sphshell_flt = pd.concat([df_sphshell_rejected, df_sphshell_fixed])

#################################################
# model = trpowerlawmod
#################################################

query_pnfit_results_trpowerlawmod = PneFitResults.select(). \
    where(PneFitResults.model == 'trpowerlawmod',
          PneFitResults.theta.is_null(False),
          PneFitResults.freq_0_start < PneFitResults.freq_0 * select_nu0_factor,
          PneFitResults.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_trpowerlawmod = pd.DataFrame(list(query_pnfit_results_trpowerlawmod.dicts()))[select_fit]

# df_trpowerlawmod correction for 10% of the maximum
df_trpowerlawmod['theta'] = df_trpowerlawmod['theta'] * 1.8368

# EM and EM uncertainty for trpowerlawmod
EM_unc = unumpy.uarray(df_trpowerlawmod.freq_0, df_trpowerlawmod.freq_0_err)
EM = ibph.EM(EM_unc, 1.E4)
df_trpowerlawmod['EM'] = pd.Series(unumpy.nominal_values(EM))
df_trpowerlawmod['EM_err'] = pd.Series(unumpy.std_devs(EM))

#################################################
# powerlaw
#################################################

select_powerlaw = ['idPNMain', 'model', 'fittype', 'intersect', 'intersect_err', 'power', 'power_err', 'mse',
                   'n_points', 'nrmse', 'mape', 'smape', 'redchisqg']

#################################################
# model = powerlaw crossection
query_pnfit_results_powerlaw_cross = PneFitResults.select(). \
    where(PneFitResults.model == 'powerlaw',
          PneFitResults.fittype == 'curvefit',
          PneFitResults.power.is_null(False),
          PneFitResults.idPNMain << df_sphshell.idPNMain.values.tolist(),
          PneFitResults.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_powerlaw_cross = pd.DataFrame(list(query_pnfit_results_powerlaw_cross.dicts()))[select_powerlaw]

#################################################
# model = powerlaw not fitted with sphshell
query_pnfit_results_powerlaw = PneFitResults.select(). \
    where(PneFitResults.model == 'powerlaw',
          PneFitResults.fittype == 'curvefit',
          PneFitResults.power.is_null(False),
          ~(PneFitResults.idPNMain << df_sphshell.idPNMain.values.tolist()),
          PneFitResults.idPNMain << df_meta_limited.idPNMain.values.tolist())
df_powerlaw = pd.DataFrame(list(query_pnfit_results_powerlaw.dicts()))[select_powerlaw]

#################################################
# merge data
# sphshell has extension x (i.e. _x)
# trpowerlawmod has y (i.e. _y)
#################################################

# merge sphshell and trpowerlawmod
df_results_merged = pd.merge(df_sphshell, df_trpowerlawmod, on='idPNMain', how='outer')

# merge fit results and distances
df_results_merged = pd.merge(df_results_merged, df_distances, on='idPNMain', how='left')

# merge fit results and meta (hash)
df_results_merged = pd.merge(df_results_merged, df_meta_limited, on='idPNMain', how='left')

# merge all radio data and meta (hash)
df_radio_all_merged = pd.merge(df_radio_data, df_meta_limited, on='idPNMain', how='left')

#################################################
# fit quality
#################################################

# smape binning
# df_results_merged['qsmape'] = pd.cut(df_results_merged.smape_x,2,labels=[1,2])#range(1,4))
# bins = pd.IntervalIndex.from_tuples([(0, 5), (5, 10), (10, 100)])
bins = [0, 10, 30, 100]
df_results_merged['qsmape_x'] = pd.cut(df_results_merged.nrmse_x, bins=bins, labels=[1, 2, 3])  # range(1,4))
df_results_merged['quality_x'] = df_results_merged.apply(lambda row: noqual(row, df_radio_data, 'freq_0_x', 'qsmape_x'),
                                                         axis=1)

df_results_merged['qsmape_y'] = pd.cut(df_results_merged.nrmse_y, bins=bins, labels=[1, 2, 3])  # range(1,4))
df_results_merged['quality_y'] = df_results_merged.apply(lambda row: noqual(row, df_radio_data, 'freq_0_y', 'qsmape_y'),
                                                         axis=1)
# # smape binning
# # df_results_merged['qsmape'] = pd.cut(df_results_merged.smape_x,2,labels=[1,2])#range(1,4))
# # bins = pd.IntervalIndex.from_tuples([(0, 5), (5, 10), (10, 100)])
# bins = [0, 0.1, 0.3, 1]
# df_results_merged['qsmape_x'] = pd.cut(df_results_merged.smape_x, bins=bins, labels=[1, 2, 3])  # range(1,4))
# df_results_merged['quality_x'] = df_results_merged.apply(lambda row: noqual(row, df_radio_data, 'freq_0_x', 'qsmape_x'),
#                                                          axis=1)
#
# df_results_merged['qsmape_y'] = pd.cut(df_results_merged.smape_y, bins=bins, labels=[1, 2, 3])  # range(1,4))
# df_results_merged['quality_y'] = df_results_merged.apply(lambda row: noqual(row, df_radio_data, 'freq_0_y', 'qsmape_y'),
#                                                          axis=1)

#################################################
# physical radii from distance and fitted diameter
#################################################

# uncertainty array of distances (value and unc.)
distances = unumpy.uarray(df_results_merged.d, (df_results_merged.d_errup + df_results_merged.d_errdown) / 2)

# unc. array of sphshell diams
theta_x = unumpy.uarray(df_results_merged.theta_x, df_results_merged.theta_err_x)
# physical radii for sphshell
PhRad_x = ibph.radius(distances, theta_x)[0] / 1000
df_results_merged['PhRad_x'] = pd.Series(unumpy.nominal_values(PhRad_x))
df_results_merged['PhRad_err_x'] = pd.Series(unumpy.std_devs(PhRad_x))

# unc. array of trpowerlawmod diams
theta_y = unumpy.uarray(df_results_merged.theta_y, df_results_merged.theta_err_y)
# physical radii for trpowerlawmod
PhRad_y = ibph.radius(distances, theta_y)[0] / 1000
df_results_merged['PhRad_y'] = pd.Series(unumpy.nominal_values(PhRad_y))
df_results_merged['PhRad_err_y'] = pd.Series(unumpy.std_devs(PhRad_y))


#################################################
# merge sed fitted diams with literature diams
#################################################

df_full_data = pd.merge(df_all_diams, df_results_merged, on='idPNMain', how='outer')

#################################################
# add diff column (DiamRad - theta)/DiamRad
# and min and max column for plotting uncertanty
#################################################

df_full_data['difDiams'] = df_full_data.apply(lambda row: abs(row.DiamRad - row.theta_x) / row.DiamRad,
                                              axis=1)
# uncertanties in fitted diams for sphshell
df_full_data['theta_min_x'] = df_full_data['theta_x'] - df_full_data['theta_err_x']
df_full_data['theta_max_x'] = df_full_data['theta_x'] + df_full_data['theta_err_x']

# uncertanties in fitted diams for trpowerlawmod
df_full_data['theta_min_y'] = df_full_data['theta_y'] - df_full_data['theta_err_y']
df_full_data['theta_max_y'] = df_full_data['theta_y'] + df_full_data['theta_err_y']

#################################################
# SUBSAMPLES
#################################################

# TODO
# select fits with quality below limit
# only applied to sphshell
df_full_data_qlimited = df_full_data[df_full_data.quality_x < select_limit_fit_quality]

# select not None physical radii
df_thetatheta_emr = df_full_data_qlimited[~(df_full_data_qlimited.PhRad_x.isna())]

# sample with no literature radio diam
df_thetatheta_emr_new = df_full_data_qlimited[df_full_data_qlimited.difDiams.isna()]

df_thetatheta_emr_use = df_thetatheta_emr[df_thetatheta_emr.difDiams * 0.93 <= 0.55]
df_thetatheta_emr_nouse = df_thetatheta_emr[df_thetatheta_emr.difDiams * 0.93 > 0.55]

#
