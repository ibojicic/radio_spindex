import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import pandas as pd
from importlib import reload
import make_diam_image

import load_data as ld

model = reload(model)
make_diam_image = reload(make_diam_image)

tbl_results = model.PneFitResults
query_results = tbl_results.select().where(tbl_results.model == 'sphshell')
df_results = pd.DataFrame(list(query_results.dicts()))

all_ids = df_results.idPNMain.unique()
all_ids.sort()

query_meta = Gpnfullview.select(
    Gpnfullview.png,
    Gpnfullview.idPNMain,
    Gpnfullview.draj2000,
    Gpnfullview.ddecj2000,
    Gpnfullview.PNstat,
    Gpnfullview.name,
    Gpnfullview.maj_diam,
    Gpnfullview.min_diam,
    Gpnfullview.reftb_ang_diam
)

df_meta = pd.DataFrame(list(query_meta.dicts()))

all_ids = [273]

id_lim = 1

for curr_id in all_ids:

    if curr_id < id_lim:
        continue
    print("Working on {}".format(curr_id))

    data_meta = df_meta.loc[df_meta.idPNMain == curr_id]

    radio_diams = ld.df_radio_diams.loc[ld.df_radio_diams.idPNMain == curr_id]

    fit_results = df_results.loc[df_results.idPNMain == curr_id, ['theta', 'mu', 'model']].iloc[0]

    make_diam_image.make_diam_image(curr_id, data_meta, fit_results, radio_diams)