import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import pandas as pd
import numpy as np
from importlib import reload
from ibplotting import PlotFacetSed
import load_data as ld
import sys

ld = reload(ld)
model = reload(model)
PlotFacetSed = reload(PlotFacetSed)

import sys


def makelabel(row):
    return "#{}".format(row['idPNMain'])


fullimage_height = 0.85 * 29.7/5
fullimage_width = 21.

no_rows = 1
no_cols = 3

heigh_per_row = fullimage_height / no_rows
width_per_col = fullimage_width / no_cols

per_page = no_cols * no_rows

path = "/Users/ibojicic/data/papers/SEDPNe_new/"

models_config = {
    'sphshell': {
        'linetype': '-',
        'color'   : '#990000',
    },
    'trpowerlawmod': {
        'linetype': 'dashdot',
        'color'   : '#006600',
    },
    # 'cylindrical'  : {
    #     'linetype': '--',
    #     'color'   : '#000066',
    # },
    # 'powerlaw': {
    #     'linetype': 'dotted',
    #     'color'   : '#000066'
    # }

}

df_results_merged = ld.df_full_data[~ld.df_full_data.theta_x.isna()].copy()

# (ld.df_results_merged.quality_x == 3)

tbl_results = model.PneFitResults

# query_results = tbl_results.select().where(tbl_results.idPNMain << list(df_results_merged.idPNMain))
query_results = tbl_results.select().where(tbl_results.idPNMain << [483, 965, 4103])
df_results_all = pd.DataFrame(list(query_results.dicts()))

df_results = df_results_all[df_results_all.fittype != 'curvefit'].copy()
all_ids = df_results.idPNMain.unique()

tbl_radio_data = model.RadioData
query_radio = tbl_radio_data.select().where((tbl_radio_data.InUse == 'y') & (tbl_radio_data.idPNMain << list(all_ids)))
df_radio = pd.DataFrame(list(query_radio.dicts()))
df_radio = pd.merge(df_radio, df_results_merged[['idPNMain', 'quality_x']], on='idPNMain', how='left')

df_radio['imlabel'] = df_radio.apply(lambda row: makelabel(row), axis=1)

df_radio.flux_err.fillna(df_radio.flux / 10., inplace=True)

df_radio['flux_min'] = df_radio['flux'] - df_radio['flux_err']
df_radio.loc[df_radio['flux_min'] < 0, 'flux_min'] = 1.e-3
df_radio['flux_max'] = df_radio['flux'] + df_radio['flux_err']

df_radio['medianflux'] = df_radio[['idPNMain', 'flux']].groupby('idPNMain')['flux'].transform('median')

for_sorting = df_radio[['idPNMain', 'quality_x', 'medianflux']].drop_duplicates().sort_values(
    ['quality_x', 'medianflux'])

sorted_by_flux = np.array(pd.unique(for_sorting.idPNMain))

n = len(sorted_by_flux)

rem = n % per_page

# sys.exit()

if rem != 0:
    sorted_by_flux = np.pad(sorted_by_flux, (0, per_page - rem), 'constant')

groups = sorted_by_flux.reshape([-1, per_page])

group_id = 0

for pageids in groups:
    pageids = pageids[pageids > 0]
    group_id += 1

    df_radio_group = df_radio.set_index('idPNMain').loc[pageids].reset_index()
    df_results_group = df_results.set_index('idPNMain').loc[pageids].reset_index()

    newplt = PlotFacetSed.PlotFacetSed()
    newplt.set_config_pars(xlabel='Freq (GHz)',
                           ylabel='Flux (mJy)'
                           )
    newplt.base_data = df_radio_group
    newplt.facet_SEDs(df_results_group, models_config, pageids, nrocol=no_cols)
    newplt.ylimits = (10, 200)


    newplt = newplt.save_plot(newplt.plot_out(), "{}plots/gradient.eps".format(path),
                              heightim=heigh_per_row , widthim=fullimage_width, units='cm', )

