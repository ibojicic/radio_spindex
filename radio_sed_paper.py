import peeweedbmodels.dbSED_PNe as dbSED_PNe
import pandas as pd
from ibfitting import ThermalSed, PowerLaw

tbl_radio_data = dbSED_PNe.RadioData
tbl_results = dbSED_PNe.PneFitResults

query_radio = tbl_radio_data.select()
df_radio = pd.DataFrame(list(query_radio.dicts()))

all_ids = df_radio.idPNMain.unique()

all_ids.sort()

all_ids = [401]
models = ['sphshell', 'trpowerlawmod', 'cylindrical']
# models = ['trpowerlawmod']
linearmodels = ['curvefit', 'curvefitfixed']
# linearmodels = []
# models = []
record_to_db = True
id_lim = 1

for curr_id in all_ids:

    if curr_id < id_lim:
        continue
    print("Working on {}".format(curr_id))
    # select specific object
    # for fitting use only InUse = 'y'
    data_radio = df_radio.loc[(df_radio['idPNMain'] == curr_id) & (df_radio['InUse'] == 'y'),].copy()

    # for unavailable uncertanties use 10%
    data_radio.flux_err.fillna(data_radio.flux / 10., inplace=True)

    unq_bands = data_radio.band.unique()

    if len(unq_bands) < 3:
        continue

    # linear fit
    for linmdl in linearmodels:
        fit_params = {
            "model"  : 'powerlaw',
            "fittype": linmdl
        }

        linfitter = PowerLaw.PowerLaw(fit_params)
        linfitter.set_data_pandas(data_radio, xcol='freq', ycol='flux', ycol_err='flux_err')
        linfitter.run_fit()

        if record_to_db:
            tbl_results.create(idPNMain=curr_id, **linfitter.full_results)

    for mdl in models:
        fit_params = {
            "model": mdl,
            "Te"   : 1.E4,
            "mu"   : 0.4
        }

        fitter = ThermalSed.ThermalSed(fit_params)

        fitter.set_data_pandas(data_radio, xcol='freq', ycol='flux', ycol_err='flux_err', beams='bmaj')
        fitter.run_fit(absolute_sigma=True)
        if record_to_db:
            tbl_results.create(idPNMain=curr_id, **fitter.full_results)
