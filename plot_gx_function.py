import numpy as np
import pandas as pd
from ibastro import thermal_sed
from ibplotting import PlotBasics
from scipy.integrate import simps

## PLOT OF gx functions Onlon 1975 and Bojicic et al. 2019-20?

path_to_paper = "/Users/ibojicic/data/papers/SEDPNe_new/plots/"

## ni = Rin/R Scohnberner et al. 2007
mu = 0.4

## True normalize to area under curve = 1
## False normalize to peak = 1
flag_normalize = True

# spherical shell
xseq_1 = np.arange(0., mu, 0.01)
xseq_2 = np.arange(mu, 1, 0.01)
xseq_sph = np.append(xseq_1, xseq_2)

sphshell_fnc1 = thermal_sed.gx_sphshell(xseq_1, mu) / (1 - mu)
sphshell_fnc2 = thermal_sed.gx_sphere(xseq_2) / (1 - mu)

sphshell_fnc = np.append(sphshell_fnc1, sphshell_fnc2)

sphshell = pd.DataFrame({'x': xseq_sph, 'y': sphshell_fnc})

# pwlawmod
xseq_pwl = np.arange(0., 2, 0.0001)
pwlawmod_val = []

for x in xseq_pwl:
    pwlawmod_val.append(thermal_sed.gx_pwlawmod(x, mu) / (8 / 3 - 2 * mu))

pwlawmod = pd.DataFrame({'x': xseq_pwl, 'y': pwlawmod_val})

plot = PlotBasics.PlotBasics("linlin")
plot.set_config_pars(xlabel='x', ylabel='normalised g(x)')
plot.data = sphshell
plot.plot_line("x", "y", linetype='-')
plot.data = pwlawmod
plot.plot_line("x", "y", linetype='dashdot')
plot.save_plot(plot.plot_out(), "{}gx_plot.eps".format(path_to_paper))
