import peeweedbmodels.dbSED_PNe as model
from peeweedbmodels.dbMainGPN import Gpnfullview
import numpy as np
import pandas as pd
from importlib import reload
from ibfitting import ThermalSed, PowerLaw
from ibplotting import PlotSed

model = reload(model)
ThermalSed = reload(ThermalSed)
PowerLaw = reload(PowerLaw)
PlotSed = reload(PlotSed)

tbl_results = model.PneFitResults
query_results = tbl_results.select()
df_results = pd.DataFrame(list(query_results.dicts()))

all_ids = df_results.idPNMain.unique()
all_ids.sort()

tbl_radio_data = model.RadioData
query_radio = tbl_radio_data.select()
df_radio = pd.DataFrame(list(query_radio.dicts()))

query_meta = Gpnfullview.select(
    Gpnfullview.png,
    Gpnfullview.idPNMain,
    Gpnfullview.draj2000,
    Gpnfullview.ddecj2000,
    Gpnfullview.p_nstatus,
    Gpnfullview.name,
    Gpnfullview.maj_diam,
    Gpnfullview.min_diam,
    Gpnfullview.reftb_ang_diam
)

df_meta = pd.DataFrame(list(query_meta.dicts()))

# all_ids = [94, 202, 325, 388, 406, 501, 639, 730, 741, 752, 775, 780, 824, 1097, 1144]
# all_ids = [273]
all_ids = [401]

models = ['sphshell', 'trpowerlawmod', 'cylindrical']
# models = ['sphshell']

id_lim = 1

models_config = {
    'sphshell'     : {
        'lntp'    : '-',
        'clr'     : '#990000',
        'plt_conf': True},
    'trpowerlawmod': {
        'lntp'    : 'dashdot',
        'clr'     : '#006600',
        'plt_conf': False},
    'cylindrical'  : {
        'lntp'    : '--',
        'clr'     : '#000066',
        'plt_conf': False}
}

for curr_id in all_ids:

    if curr_id < id_lim:
        continue
    print("Working on {}".format(curr_id))
    # select specific object
    # for fitting use only InUse = 'y'
    data_radio = df_radio.loc[(df_radio['idPNMain'] == curr_id) & (df_radio['InUse'] == 'y'),]
    data_meta = df_meta.loc[df_meta['idPNMain'] == curr_id]

    # for unavailable uncertanties use 10%
    data_radio.flux_err.fillna(data_radio.flux / 10., inplace=True)

    unq_bands = data_radio.band.unique()

    if len(unq_bands) < 3:
        continue

    newnewplot = PlotSed.PlotSed()
    newnewplot.set_config_pars(xlabel='Freq (GHz)', ylabel='Flux (mJy)')
    newnewplot.set_limits_on_data(data_radio.freq, data_radio.flux)
    newnewplot.plt_data_points(data_radio, True)

    fit_results_lin = df_results.loc[
        (df_results.idPNMain == curr_id) & (df_results.model == "powerlaw") & (df_results.fittype == "curvefitfixed"),
        ['intersect', 'power']]
    if not np.isnan(fit_results_lin.intersect).all():
        newnewplot.plot_pwr_law(data_radio.freq, **fit_results_lin.to_dict('r')[0], linetype='dotted')

    for mdl in models_config:
        fit_results = df_results.loc[
            (df_results.idPNMain == curr_id) & (df_results.model == mdl), ['theta', 'freq_0', 'mu', 'Te', 'model']]
        fit_errors = df_results.loc[
            (df_results.idPNMain == curr_id) & (df_results.model == mdl), ['theta_err', 'freq_0_err']]
        if np.isnan(fit_results.theta).all():
            continue
        newnewplot.plt_thermal_sed(data_radio.freq,
                                   fit_results.to_dict('r')[0],
                                   fitted_err=fit_errors.to_dict('r')[0],
                                   **models_config[mdl])

    newnewplot.save_plot(newnewplot.plot_out(),
                         "/Users/ibojicic/data/PROJECTS/astro/sed_pne_paper/SMC_sedplots/{}_sed.png".format(
                             curr_id))
